# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AddressBar>` | `<address-bar>` (components/addressBar.vue)
- `<AgentBox>` | `<agent-box>` (components/agentBox.vue)
- `<CommentCreate>` | `<comment-create>` (components/commentCreate.js)
- `<FreeBox>` | `<free-box>` (components/freeBox.vue)
- `<HomeBanner>` | `<home-banner>` (components/HomeBanner.vue)
- `<LoadingView>` | `<loading-view>` (components/LoadingView.vue)
- `<NotificationService>` | `<notification-service>` (components/notificationService.js)
- `<SlideRight>` | `<slide-right>` (components/slideRight.vue)
- `<Auth>` | `<auth>` (components/AuthIndex/Auth.vue)
- `<AuthIndexLogin>` | `<auth-index-login>` (components/AuthIndex/Login.vue)
- `<AuthIndexRegister>` | `<auth-index-register>` (components/AuthIndex/Register.vue)
- `<CardAddress>` | `<card-address>` (components/card/address.vue)
- `<CardBack>` | `<card-back>` (components/card/cardBack.vue)
- `<CardCon>` | `<card-con>` (components/card/cardCon.vue)
- `<CardStatus>` | `<card-status>` (components/card/cardStatus.vue)
- `<CardEndPrice>` | `<card-end-price>` (components/card/endPrice.vue)
- `<CardTotalPrice>` | `<card-total-price>` (components/card/totalPrice.vue)
- `<Header>` | `<header>` (components/header/header.vue)
- `<HeaderSearch>` | `<header-search>` (components/header/search.vue)
- `<Footer>` | `<footer>` (components/footer/footer.vue)
- `<CategoryList>` | `<category-list>` (components/category/categoryList.vue)
- `<ProductInfo>` | `<product-info>` (components/product/info.vue)
- `<ProductNoAvailable>` | `<product-no-available>` (components/product/noAvailable.vue)
- `<ProductPrice>` | `<product-price>` (components/product/price.vue)
- `<ProductComment>` | `<product-comment>` (components/product/productComment.vue)
- `<ProductName>` | `<product-name>` (components/product/productName.vue)
- `<ProductOption>` | `<product-option>` (components/product/productOption.vue)
- `<ProductOrder>` | `<product-order>` (components/product/productOrder.vue)
- `<ProductQuestion>` | `<product-question>` (components/product/productQuestion.vue)
- `<ProductRelated>` | `<product-related>` (components/product/productRelated.vue)
- `<ProductSlide>` | `<product-slide>` (components/product/productSlide.vue)
- `<ProductSpecial>` | `<product-special>` (components/product/productSpecial.vue)
- `<ProductView>` | `<product-view>` (components/product/productView.vue)
- `<ProfileLastCards>` | `<profile-last-cards>` (components/profile/lastCards.vue)
- `<ProfileMenu>` | `<profile-menu>` (components/profile/profileMenu.vue)
- `<ServiceComment>` | `<service-comment>` (components/Service/serviceComment.vue)
- `<ServiceImage>` | `<service-image>` (components/Service/serviceImage.vue)
