import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  AddressBar: () => import('../..\\components\\addressBar.vue' /* webpackChunkName: "components/address-bar" */).then(c => wrapFunctional(c.default || c)),
  AgentBox: () => import('../..\\components\\agentBox.vue' /* webpackChunkName: "components/agent-box" */).then(c => wrapFunctional(c.default || c)),
  CommentCreate: () => import('../..\\components\\commentCreate.js' /* webpackChunkName: "components/comment-create" */).then(c => wrapFunctional(c.default || c)),
  FreeBox: () => import('../..\\components\\freeBox.vue' /* webpackChunkName: "components/free-box" */).then(c => wrapFunctional(c.default || c)),
  HomeBanner: () => import('../..\\components\\HomeBanner.vue' /* webpackChunkName: "components/home-banner" */).then(c => wrapFunctional(c.default || c)),
  LoadingView: () => import('../..\\components\\LoadingView.vue' /* webpackChunkName: "components/loading-view" */).then(c => wrapFunctional(c.default || c)),
  NotificationService: () => import('../..\\components\\notificationService.js' /* webpackChunkName: "components/notification-service" */).then(c => wrapFunctional(c.default || c)),
  SlideRight: () => import('../..\\components\\slideRight.vue' /* webpackChunkName: "components/slide-right" */).then(c => wrapFunctional(c.default || c)),
  Auth: () => import('../..\\components\\AuthIndex\\Auth.vue' /* webpackChunkName: "components/auth" */).then(c => wrapFunctional(c.default || c)),
  AuthIndexLogin: () => import('../..\\components\\AuthIndex\\Login.vue' /* webpackChunkName: "components/auth-index-login" */).then(c => wrapFunctional(c.default || c)),
  AuthIndexRegister: () => import('../..\\components\\AuthIndex\\Register.vue' /* webpackChunkName: "components/auth-index-register" */).then(c => wrapFunctional(c.default || c)),
  CardAddress: () => import('../..\\components\\card\\address.vue' /* webpackChunkName: "components/card-address" */).then(c => wrapFunctional(c.default || c)),
  CardBack: () => import('../..\\components\\card\\cardBack.vue' /* webpackChunkName: "components/card-back" */).then(c => wrapFunctional(c.default || c)),
  CardCon: () => import('../..\\components\\card\\cardCon.vue' /* webpackChunkName: "components/card-con" */).then(c => wrapFunctional(c.default || c)),
  CardStatus: () => import('../..\\components\\card\\cardStatus.vue' /* webpackChunkName: "components/card-status" */).then(c => wrapFunctional(c.default || c)),
  CardEndPrice: () => import('../..\\components\\card\\endPrice.vue' /* webpackChunkName: "components/card-end-price" */).then(c => wrapFunctional(c.default || c)),
  CardTotalPrice: () => import('../..\\components\\card\\totalPrice.vue' /* webpackChunkName: "components/card-total-price" */).then(c => wrapFunctional(c.default || c)),
  Header: () => import('../..\\components\\header\\header.vue' /* webpackChunkName: "components/header" */).then(c => wrapFunctional(c.default || c)),
  HeaderSearch: () => import('../..\\components\\header\\search.vue' /* webpackChunkName: "components/header-search" */).then(c => wrapFunctional(c.default || c)),
  Footer: () => import('../..\\components\\footer\\footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c)),
  CategoryList: () => import('../..\\components\\category\\categoryList.vue' /* webpackChunkName: "components/category-list" */).then(c => wrapFunctional(c.default || c)),
  ProductInfo: () => import('../..\\components\\product\\info.vue' /* webpackChunkName: "components/product-info" */).then(c => wrapFunctional(c.default || c)),
  ProductNoAvailable: () => import('../..\\components\\product\\noAvailable.vue' /* webpackChunkName: "components/product-no-available" */).then(c => wrapFunctional(c.default || c)),
  ProductPrice: () => import('../..\\components\\product\\price.vue' /* webpackChunkName: "components/product-price" */).then(c => wrapFunctional(c.default || c)),
  ProductComment: () => import('../..\\components\\product\\productComment.vue' /* webpackChunkName: "components/product-comment" */).then(c => wrapFunctional(c.default || c)),
  ProductName: () => import('../..\\components\\product\\productName.vue' /* webpackChunkName: "components/product-name" */).then(c => wrapFunctional(c.default || c)),
  ProductOption: () => import('../..\\components\\product\\productOption.vue' /* webpackChunkName: "components/product-option" */).then(c => wrapFunctional(c.default || c)),
  ProductOrder: () => import('../..\\components\\product\\productOrder.vue' /* webpackChunkName: "components/product-order" */).then(c => wrapFunctional(c.default || c)),
  ProductQuestion: () => import('../..\\components\\product\\productQuestion.vue' /* webpackChunkName: "components/product-question" */).then(c => wrapFunctional(c.default || c)),
  ProductRelated: () => import('../..\\components\\product\\productRelated.vue' /* webpackChunkName: "components/product-related" */).then(c => wrapFunctional(c.default || c)),
  ProductSlide: () => import('../..\\components\\product\\productSlide.vue' /* webpackChunkName: "components/product-slide" */).then(c => wrapFunctional(c.default || c)),
  ProductSpecial: () => import('../..\\components\\product\\productSpecial.vue' /* webpackChunkName: "components/product-special" */).then(c => wrapFunctional(c.default || c)),
  ProductView: () => import('../..\\components\\product\\productView.vue' /* webpackChunkName: "components/product-view" */).then(c => wrapFunctional(c.default || c)),
  ProfileLastCards: () => import('../..\\components\\profile\\lastCards.vue' /* webpackChunkName: "components/profile-last-cards" */).then(c => wrapFunctional(c.default || c)),
  ProfileMenu: () => import('../..\\components\\profile\\profileMenu.vue' /* webpackChunkName: "components/profile-menu" */).then(c => wrapFunctional(c.default || c)),
  ServiceComment: () => import('../..\\components\\Service\\serviceComment.vue' /* webpackChunkName: "components/service-comment" */).then(c => wrapFunctional(c.default || c)),
  ServiceImage: () => import('../..\\components\\Service\\serviceImage.vue' /* webpackChunkName: "components/service-image" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
