import { wrapFunctional } from './utils'

export { default as AddressBar } from '../..\\components\\addressBar.vue'
export { default as AgentBox } from '../..\\components\\agentBox.vue'
export { default as CommentCreate } from '../..\\components\\commentCreate.js'
export { default as FreeBox } from '../..\\components\\freeBox.vue'
export { default as HomeBanner } from '../..\\components\\HomeBanner.vue'
export { default as LoadingView } from '../..\\components\\LoadingView.vue'
export { default as NotificationService } from '../..\\components\\notificationService.js'
export { default as SlideRight } from '../..\\components\\slideRight.vue'
export { default as Auth } from '../..\\components\\AuthIndex\\Auth.vue'
export { default as AuthIndexLogin } from '../..\\components\\AuthIndex\\Login.vue'
export { default as AuthIndexRegister } from '../..\\components\\AuthIndex\\Register.vue'
export { default as CardAddress } from '../..\\components\\card\\address.vue'
export { default as CardBack } from '../..\\components\\card\\cardBack.vue'
export { default as CardCon } from '../..\\components\\card\\cardCon.vue'
export { default as CardStatus } from '../..\\components\\card\\cardStatus.vue'
export { default as CardEndPrice } from '../..\\components\\card\\endPrice.vue'
export { default as CardTotalPrice } from '../..\\components\\card\\totalPrice.vue'
export { default as Header } from '../..\\components\\header\\header.vue'
export { default as HeaderSearch } from '../..\\components\\header\\search.vue'
export { default as Footer } from '../..\\components\\footer\\footer.vue'
export { default as CategoryList } from '../..\\components\\category\\categoryList.vue'
export { default as ProductInfo } from '../..\\components\\product\\info.vue'
export { default as ProductNoAvailable } from '../..\\components\\product\\noAvailable.vue'
export { default as ProductPrice } from '../..\\components\\product\\price.vue'
export { default as ProductComment } from '../..\\components\\product\\productComment.vue'
export { default as ProductName } from '../..\\components\\product\\productName.vue'
export { default as ProductOption } from '../..\\components\\product\\productOption.vue'
export { default as ProductOrder } from '../..\\components\\product\\productOrder.vue'
export { default as ProductQuestion } from '../..\\components\\product\\productQuestion.vue'
export { default as ProductRelated } from '../..\\components\\product\\productRelated.vue'
export { default as ProductSlide } from '../..\\components\\product\\productSlide.vue'
export { default as ProductSpecial } from '../..\\components\\product\\productSpecial.vue'
export { default as ProductView } from '../..\\components\\product\\productView.vue'
export { default as ProfileLastCards } from '../..\\components\\profile\\lastCards.vue'
export { default as ProfileMenu } from '../..\\components\\profile\\profileMenu.vue'
export { default as ServiceComment } from '../..\\components\\Service\\serviceComment.vue'
export { default as ServiceImage } from '../..\\components\\Service\\serviceImage.vue'

export const LazyAddressBar = import('../..\\components\\addressBar.vue' /* webpackChunkName: "components/address-bar" */).then(c => wrapFunctional(c.default || c))
export const LazyAgentBox = import('../..\\components\\agentBox.vue' /* webpackChunkName: "components/agent-box" */).then(c => wrapFunctional(c.default || c))
export const LazyCommentCreate = import('../..\\components\\commentCreate.js' /* webpackChunkName: "components/comment-create" */).then(c => wrapFunctional(c.default || c))
export const LazyFreeBox = import('../..\\components\\freeBox.vue' /* webpackChunkName: "components/free-box" */).then(c => wrapFunctional(c.default || c))
export const LazyHomeBanner = import('../..\\components\\HomeBanner.vue' /* webpackChunkName: "components/home-banner" */).then(c => wrapFunctional(c.default || c))
export const LazyLoadingView = import('../..\\components\\LoadingView.vue' /* webpackChunkName: "components/loading-view" */).then(c => wrapFunctional(c.default || c))
export const LazyNotificationService = import('../..\\components\\notificationService.js' /* webpackChunkName: "components/notification-service" */).then(c => wrapFunctional(c.default || c))
export const LazySlideRight = import('../..\\components\\slideRight.vue' /* webpackChunkName: "components/slide-right" */).then(c => wrapFunctional(c.default || c))
export const LazyAuth = import('../..\\components\\AuthIndex\\Auth.vue' /* webpackChunkName: "components/auth" */).then(c => wrapFunctional(c.default || c))
export const LazyAuthIndexLogin = import('../..\\components\\AuthIndex\\Login.vue' /* webpackChunkName: "components/auth-index-login" */).then(c => wrapFunctional(c.default || c))
export const LazyAuthIndexRegister = import('../..\\components\\AuthIndex\\Register.vue' /* webpackChunkName: "components/auth-index-register" */).then(c => wrapFunctional(c.default || c))
export const LazyCardAddress = import('../..\\components\\card\\address.vue' /* webpackChunkName: "components/card-address" */).then(c => wrapFunctional(c.default || c))
export const LazyCardBack = import('../..\\components\\card\\cardBack.vue' /* webpackChunkName: "components/card-back" */).then(c => wrapFunctional(c.default || c))
export const LazyCardCon = import('../..\\components\\card\\cardCon.vue' /* webpackChunkName: "components/card-con" */).then(c => wrapFunctional(c.default || c))
export const LazyCardStatus = import('../..\\components\\card\\cardStatus.vue' /* webpackChunkName: "components/card-status" */).then(c => wrapFunctional(c.default || c))
export const LazyCardEndPrice = import('../..\\components\\card\\endPrice.vue' /* webpackChunkName: "components/card-end-price" */).then(c => wrapFunctional(c.default || c))
export const LazyCardTotalPrice = import('../..\\components\\card\\totalPrice.vue' /* webpackChunkName: "components/card-total-price" */).then(c => wrapFunctional(c.default || c))
export const LazyHeader = import('../..\\components\\header\\header.vue' /* webpackChunkName: "components/header" */).then(c => wrapFunctional(c.default || c))
export const LazyHeaderSearch = import('../..\\components\\header\\search.vue' /* webpackChunkName: "components/header-search" */).then(c => wrapFunctional(c.default || c))
export const LazyFooter = import('../..\\components\\footer\\footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c))
export const LazyCategoryList = import('../..\\components\\category\\categoryList.vue' /* webpackChunkName: "components/category-list" */).then(c => wrapFunctional(c.default || c))
export const LazyProductInfo = import('../..\\components\\product\\info.vue' /* webpackChunkName: "components/product-info" */).then(c => wrapFunctional(c.default || c))
export const LazyProductNoAvailable = import('../..\\components\\product\\noAvailable.vue' /* webpackChunkName: "components/product-no-available" */).then(c => wrapFunctional(c.default || c))
export const LazyProductPrice = import('../..\\components\\product\\price.vue' /* webpackChunkName: "components/product-price" */).then(c => wrapFunctional(c.default || c))
export const LazyProductComment = import('../..\\components\\product\\productComment.vue' /* webpackChunkName: "components/product-comment" */).then(c => wrapFunctional(c.default || c))
export const LazyProductName = import('../..\\components\\product\\productName.vue' /* webpackChunkName: "components/product-name" */).then(c => wrapFunctional(c.default || c))
export const LazyProductOption = import('../..\\components\\product\\productOption.vue' /* webpackChunkName: "components/product-option" */).then(c => wrapFunctional(c.default || c))
export const LazyProductOrder = import('../..\\components\\product\\productOrder.vue' /* webpackChunkName: "components/product-order" */).then(c => wrapFunctional(c.default || c))
export const LazyProductQuestion = import('../..\\components\\product\\productQuestion.vue' /* webpackChunkName: "components/product-question" */).then(c => wrapFunctional(c.default || c))
export const LazyProductRelated = import('../..\\components\\product\\productRelated.vue' /* webpackChunkName: "components/product-related" */).then(c => wrapFunctional(c.default || c))
export const LazyProductSlide = import('../..\\components\\product\\productSlide.vue' /* webpackChunkName: "components/product-slide" */).then(c => wrapFunctional(c.default || c))
export const LazyProductSpecial = import('../..\\components\\product\\productSpecial.vue' /* webpackChunkName: "components/product-special" */).then(c => wrapFunctional(c.default || c))
export const LazyProductView = import('../..\\components\\product\\productView.vue' /* webpackChunkName: "components/product-view" */).then(c => wrapFunctional(c.default || c))
export const LazyProfileLastCards = import('../..\\components\\profile\\lastCards.vue' /* webpackChunkName: "components/profile-last-cards" */).then(c => wrapFunctional(c.default || c))
export const LazyProfileMenu = import('../..\\components\\profile\\profileMenu.vue' /* webpackChunkName: "components/profile-menu" */).then(c => wrapFunctional(c.default || c))
export const LazyServiceComment = import('../..\\components\\Service\\serviceComment.vue' /* webpackChunkName: "components/service-comment" */).then(c => wrapFunctional(c.default || c))
export const LazyServiceImage = import('../..\\components\\Service\\serviceImage.vue' /* webpackChunkName: "components/service-image" */).then(c => wrapFunctional(c.default || c))
