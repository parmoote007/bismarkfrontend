import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _ba49df8e = () => interopDefault(import('..\\pages\\404.vue' /* webpackChunkName: "pages/404" */))
const _565f7943 = () => interopDefault(import('..\\pages\\card\\index.vue' /* webpackChunkName: "pages/card/index" */))
const _05bd9f2f = () => interopDefault(import('..\\pages\\index2.vue' /* webpackChunkName: "pages/index2" */))
const _2b45ed98 = () => interopDefault(import('..\\pages\\profile\\index.vue' /* webpackChunkName: "pages/profile/index" */))
const _1d17496b = () => interopDefault(import('..\\pages\\search\\index.vue' /* webpackChunkName: "pages/search/index" */))
const _402c510c = () => interopDefault(import('..\\pages\\service\\index.vue' /* webpackChunkName: "pages/service/index" */))
const _6bcfc3fa = () => interopDefault(import('..\\pages\\serviceAgent\\index.vue' /* webpackChunkName: "pages/serviceAgent/index" */))
const _a91d7c26 = () => interopDefault(import('..\\pages\\card\\cardList.vue' /* webpackChunkName: "pages/card/cardList" */))
const _d769b6f2 = () => interopDefault(import('..\\pages\\card\\cardPayment.vue' /* webpackChunkName: "pages/card/cardPayment" */))
const _67ced54e = () => interopDefault(import('..\\pages\\card\\complete-profile\\index.vue' /* webpackChunkName: "pages/card/complete-profile/index" */))
const _5d946150 = () => interopDefault(import('..\\pages\\card\\endBoy.vue' /* webpackChunkName: "pages/card/endBoy" */))
const _5845dfd7 = () => interopDefault(import('..\\pages\\card\\sendCard.vue' /* webpackChunkName: "pages/card/sendCard" */))
const _0e49ec3a = () => interopDefault(import('..\\pages\\profile\\address.vue' /* webpackChunkName: "pages/profile/address" */))
const _3f0eca81 = () => interopDefault(import('..\\pages\\service\\ServiceNew.vue' /* webpackChunkName: "pages/service/ServiceNew" */))
const _4c8fd8d5 = () => interopDefault(import('..\\pages\\card\\checkPayment\\_id\\index.vue' /* webpackChunkName: "pages/card/checkPayment/_id/index" */))
const _07c768a5 = () => interopDefault(import('..\\pages\\card\\info\\_id\\index.vue' /* webpackChunkName: "pages/card/info/_id/index" */))
const _399b9c03 = () => interopDefault(import('..\\pages\\service\\serviceOpen\\_numberFollw\\index.vue' /* webpackChunkName: "pages/service/serviceOpen/_numberFollw/index" */))
const _729b29d6 = () => interopDefault(import('..\\pages\\category\\_slug\\index.vue' /* webpackChunkName: "pages/category/_slug/index" */))
const _2e2b15c8 = () => interopDefault(import('..\\pages\\product\\_id\\index.vue' /* webpackChunkName: "pages/product/_id/index" */))
const _6357291d = () => interopDefault(import('..\\pages\\productSerial\\_agentId\\index.vue' /* webpackChunkName: "pages/productSerial/_agentId/index" */))
const _7c0d9c93 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/404",
    component: _ba49df8e,
    name: "404"
  }, {
    path: "/card",
    component: _565f7943,
    name: "card"
  }, {
    path: "/index2",
    component: _05bd9f2f,
    name: "index2"
  }, {
    path: "/profile",
    component: _2b45ed98,
    name: "profile"
  }, {
    path: "/search",
    component: _1d17496b,
    name: "search"
  }, {
    path: "/service",
    component: _402c510c,
    name: "service"
  }, {
    path: "/serviceAgent",
    component: _6bcfc3fa,
    name: "serviceAgent"
  }, {
    path: "/card/cardList",
    component: _a91d7c26,
    name: "card-cardList"
  }, {
    path: "/card/cardPayment",
    component: _d769b6f2,
    name: "card-cardPayment"
  }, {
    path: "/card/complete-profile",
    component: _67ced54e,
    name: "card-complete-profile"
  }, {
    path: "/card/endBoy",
    component: _5d946150,
    name: "card-endBoy"
  }, {
    path: "/card/sendCard",
    component: _5845dfd7,
    name: "card-sendCard"
  }, {
    path: "/profile/address",
    component: _0e49ec3a,
    name: "profile-address"
  }, {
    path: "/service/ServiceNew",
    component: _3f0eca81,
    name: "service-ServiceNew"
  }, {
    path: "/card/checkPayment/:id",
    component: _4c8fd8d5,
    name: "card-checkPayment-id"
  }, {
    path: "/card/info/:id",
    component: _07c768a5,
    name: "card-info-id"
  }, {
    path: "/service/serviceOpen/:numberFollw",
    component: _399b9c03,
    name: "service-serviceOpen-numberFollw"
  }, {
    path: "/category/:slug",
    component: _729b29d6,
    name: "category-slug"
  }, {
    path: "/product/:id",
    component: _2e2b15c8,
    name: "product-id"
  }, {
    path: "/productSerial/:agentId",
    component: _6357291d,
    name: "productSerial-agentId"
  }, {
    path: "/",
    component: _7c0d9c93,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
