exports.ids = [42];
exports.modules = {

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(102);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("1ff4c59c", content, true)

/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".v-select{position:relative;font-family:inherit}.v-select,.v-select *{box-sizing:border-box}@-webkit-keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.vs__fade-enter-active,.vs__fade-leave-active{pointer-events:none;transition:opacity .15s cubic-bezier(1,.5,.8,1)}.vs__fade-enter,.vs__fade-leave-to{opacity:0}.vs--disabled .vs__clear,.vs--disabled .vs__dropdown-toggle,.vs--disabled .vs__open-indicator,.vs--disabled .vs__search,.vs--disabled .vs__selected{cursor:not-allowed;background-color:#f8f8f8}.v-select[dir=rtl] .vs__actions{padding:0 3px 0 6px}.v-select[dir=rtl] .vs__clear{margin-left:6px;margin-right:0}.v-select[dir=rtl] .vs__deselect{margin-left:0;margin-right:2px}.v-select[dir=rtl] .vs__dropdown-menu{text-align:right}.vs__dropdown-toggle{-webkit-appearance:none;-moz-appearance:none;appearance:none;display:flex;padding:0 0 4px;background:none;border:1px solid rgba(60,60,60,.26);border-radius:4px;white-space:normal}.vs__selected-options{display:flex;flex-basis:100%;flex-grow:1;flex-wrap:wrap;padding:0 2px;position:relative}.vs__actions{display:flex;align-items:center;padding:4px 6px 0 3px}.vs--searchable .vs__dropdown-toggle{cursor:text}.vs--unsearchable .vs__dropdown-toggle{cursor:pointer}.vs--open .vs__dropdown-toggle{border-bottom-color:transparent;border-bottom-left-radius:0;border-bottom-right-radius:0}.vs__open-indicator{fill:rgba(60,60,60,.5);transform:scale(1);transition:transform .15s cubic-bezier(1,-.115,.975,.855);transition-timing-function:cubic-bezier(1,-.115,.975,.855)}.vs--open .vs__open-indicator{transform:rotate(180deg) scale(1)}.vs--loading .vs__open-indicator{opacity:0}.vs__clear{fill:rgba(60,60,60,.5);padding:0;border:0;background-color:transparent;cursor:pointer;margin-right:8px}.vs__dropdown-menu{display:block;box-sizing:border-box;position:absolute;top:calc(100% - 1px);left:0;z-index:1000;padding:5px 0;margin:0;width:100%;max-height:350px;min-width:160px;overflow-y:auto;box-shadow:0 3px 6px 0 rgba(0,0,0,.15);border:1px solid rgba(60,60,60,.26);border-top-style:none;border-radius:0 0 4px 4px;text-align:left;list-style:none;background:#fff}.vs__no-options{text-align:center}.vs__dropdown-option{line-height:1.42857143;display:block;padding:3px 20px;clear:both;color:#333;white-space:nowrap}.vs__dropdown-option:hover{cursor:pointer}.vs__dropdown-option--highlight{background:#5897fb;color:#fff}.vs__dropdown-option--disabled{background:inherit;color:rgba(60,60,60,.5)}.vs__dropdown-option--disabled:hover{cursor:inherit}.vs__selected{display:flex;align-items:center;background-color:#f0f0f0;border:1px solid rgba(60,60,60,.26);border-radius:4px;color:#333;line-height:1.4;margin:4px 2px 0;padding:0 .25em;z-index:0}.vs__deselect{display:inline-flex;-webkit-appearance:none;-moz-appearance:none;appearance:none;margin-left:4px;padding:0;border:0;cursor:pointer;background:none;fill:rgba(60,60,60,.5);text-shadow:0 1px 0 #fff}.vs--single .vs__selected{background-color:transparent;border-color:transparent}.vs--single.vs--open .vs__selected{position:absolute;opacity:.4}.vs--single.vs--searching .vs__selected{display:none}.vs__search::-webkit-search-cancel-button{display:none}.vs__search::-ms-clear,.vs__search::-webkit-search-decoration,.vs__search::-webkit-search-results-button,.vs__search::-webkit-search-results-decoration{display:none}.vs__search,.vs__search:focus{-webkit-appearance:none;-moz-appearance:none;appearance:none;line-height:1.4;font-size:1em;border:1px solid transparent;border-left:none;outline:none;margin:4px 0 0;padding:0 7px;background:none;box-shadow:none;width:0;max-width:100%;flex-grow:1;z-index:1}.vs__search::-moz-placeholder{color:inherit}.vs__search:-ms-input-placeholder{color:inherit}.vs__search::placeholder{color:inherit}.vs--unsearchable .vs__search{opacity:1}.vs--unsearchable:not(.vs--disabled) .vs__search:hover{cursor:pointer}.vs--single.vs--searching:not(.vs--open):not(.vs--loading) .vs__search{opacity:.2}.vs__spinner{align-self:center;opacity:0;font-size:5px;text-indent:-9999em;overflow:hidden;border:.9em solid hsla(0,0%,39.2%,.1);border-left-color:rgba(60,60,60,.45);transform:translateZ(0);-webkit-animation:vSelectSpinner 1.1s linear infinite;animation:vSelectSpinner 1.1s linear infinite;transition:opacity .1s}.vs__spinner,.vs__spinner:after{border-radius:50%;width:5em;height:5em}.vs--loading .vs__spinner{opacity:1}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(229);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("4dc3bea4", content, true, context)
};

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_index_vue_vue_type_style_index_0_id_24a6d304_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(190);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_index_vue_vue_type_style_index_0_id_24a6d304_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_index_vue_vue_type_style_index_0_id_24a6d304_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_index_vue_vue_type_style_index_0_id_24a6d304_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_index_vue_vue_type_style_index_0_id_24a6d304_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".error[data-v-24a6d304]{border:1.9px solid #f58!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/productSerial/_agentId/index.vue?vue&type=template&id=24a6d304&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.render)?_c('div',{staticClass:"bg-white overflow-auto",staticStyle:{"width":"100%","height":"100%","position":"fixed","top":"0","right":"0","z-index":"35"}},[_c('client-only',[_c('div',{staticClass:" p-4 m-auto col-11 col-md-5 bg-white ",staticStyle:{"border-radius":"8px"}},[_c('div',{staticClass:"d-flex justify-content-between"},[_c('p',[_vm._v("امتیاز های کسب شده")]),_vm._v(" "),_c('i',{staticClass:"material-icons",on:{"click":function($event){return _vm.readier()}}},[_vm._v("home")])]),_vm._v(" "),_c('b-progress',{staticClass:"mb-3 text-center  ",attrs:{"value":_vm.scoreCon,"max":"80","show-value":"","variant":"info","height":"1.5rem"}}),_vm._v(" "),_c('div',{staticClass:"d-flex "},[(_vm.level !== 1)?_c('i',{staticClass:"material-icons ml-2",on:{"click":function($event){_vm.level--}}},[_vm._v("forward")]):_vm._e(),_vm._v(" "),_c('h5',{staticClass:"mb-3"},[_vm._v("سامانه قرعه کشی بیسمارک")])]),_vm._v(" "),(_vm.level === 1)?_c('div',[_c('label',{staticClass:"d-block mt-2"},[_vm._v("نام کامل")]),_vm._v(" "),_c('b-input',{class:{error : _vm.error.name},model:{value:(_vm.request.name),callback:function ($$v) {_vm.$set(_vm.request, "name", $$v)},expression:"request.name"}}),_vm._v(" "),_c('label',{staticClass:"mt-2"},[_vm._v("شماره تماس")]),_vm._v(" "),_c('b-input',{class:{error : _vm.error.phoneNumber},model:{value:(_vm.request.phoneNumber),callback:function ($$v) {_vm.$set(_vm.request, "phoneNumber", $$v)},expression:"request.phoneNumber"}}),_vm._v(" "),(_vm.agentId !== true)?_c('div',[_c('label',{staticClass:"mt-2"},[_vm._v("کد نمایدگی ")]),_vm._v(" "),_c('b-input',{class:{error : _vm.error.phoneNumber},model:{value:(_vm.request.agentId),callback:function ($$v) {_vm.$set(_vm.request, "agentId", $$v)},expression:"request.agentId"}})],1):_vm._e(),_vm._v(" "),_c('label',{staticClass:"mt-3"},[_vm._v("استان")]),_vm._v(" "),_c('b-select',{class:{error : _vm.error.stateId},model:{value:(_vm.request.stateId),callback:function ($$v) {_vm.$set(_vm.request, "stateId", $$v)},expression:"request.stateId"}},_vm._l((_vm.states),function(state,index){return _c('b-select-option',{key:index,attrs:{"value":state.id}},[_vm._v("\n            "+_vm._s(state.name)+"\n          ")])}),1),_vm._v(" "),_vm._l((_vm.con),function(c,index){return _c('div',{key:index},[_c('div',{staticClass:"d-flex mt-3"},[_c('label',{},[_vm._v("محصولات ")]),_vm._v(" "),_c('p',{staticClass:"mr-2",staticStyle:{"font-size":"12px","color":"#707070"}},[_vm._v("محصولات خود را از لیست زیر انتخاب\n              کنید.")])]),_vm._v(" "),_c('v-select',{class:{error : _vm.error.products},attrs:{"options":_vm.products,"multiple":"","label":"model"},scopedSlots:_vm._u([{key:"option",fn:function(option){return [_vm._v("\n              "+_vm._s(option.model)+" "+_vm._s(option.name)+" مدل\n            ")]}}],null,true),model:{value:(_vm.request.products),callback:function ($$v) {_vm.$set(_vm.request, "products", $$v)},expression:"request.products"}})],1)}),_vm._v(" "),_c('b-button',{staticClass:"btn-success w-100 mt-4 p-2",on:{"click":function($event){return _vm.validate1('continue')}}},[_vm._v("\n          ادامه\n        ")]),_vm._v(" "),_c('b-button',{staticClass:"btn-danger w-100 mt-4 p-2",on:{"click":function($event){return _vm.validate1('end')}}},[_vm._v("\n          اتمام\n        ")])],2):_vm._e(),_vm._v(" "),(_vm.level === 2)?_c('div',[_c('label',{staticClass:"mt-2"},[_vm._v("تاریخ تولد")]),_vm._v(" "),_c('div',{class:['col-12 border rounded bg-white p-0 pt-1 pb-1',{error:_vm.error.birthDate}]},[_c('client-only',[_c('PDatePicker',{class:{error : _vm.error.stateId},attrs:{"placeholder":"تاریخ تولد انتخاب کنید","inputClass":"position-relative col-12  border-0 w-100 p-2 m-0 text-right","wrapperClass":"col-12 w-100 p-0"},model:{value:(_vm.request.birthDate),callback:function ($$v) {_vm.$set(_vm.request, "birthDate", $$v)},expression:"request.birthDate"}})],1)],1),_vm._v(" "),_c('label',{staticClass:"mt-4"},[_vm._v("تاریخ ازدواج")]),_vm._v(" "),_c('div',{class:['col-12 border rounded bg-white p-0 pt-1 pb-1',{error:_vm.error.MarriageDate}]},[_c('client-only',[_c('PDatePicker',{attrs:{"placeholder":"تاریخ ازدواج انتخاب کنید","inputClass":"position-relative col-12  border-0 w-100 p-2 m-0 text-right","wrapperClass":"col-12 w-100 p-0"},model:{value:(_vm.request.MarriageDate),callback:function ($$v) {_vm.$set(_vm.request, "MarriageDate", $$v)},expression:"request.MarriageDate"}})],1)],1),_vm._v(" "),_c('b-button',{staticClass:"btn-success w-100 mt-2 p-2",on:{"click":function($event){return _vm.validate2('continue')}}},[_vm._v("\n          ادامه\n        ")]),_vm._v(" "),_c('b-button',{staticClass:"btn-danger w-100 mt-4 p-2",on:{"click":function($event){return _vm.validate2('end')}}},[_vm._v("\n          اتمام\n        ")])],1):_vm._e(),_vm._v(" "),(_vm.level === 3)?_c('div',[_c('label',{staticClass:"mt-4"},[_vm._v("تحصیلات ")]),_vm._v(" "),_c('b-select',{class:{error : _vm.error.education},model:{value:(_vm.request.education),callback:function ($$v) {_vm.$set(_vm.request, "education", $$v)},expression:"request.education"}},[_c('b-select-option',{attrs:{"value":"1"}},[_vm._v("دیپلم")]),_vm._v(" "),_c('b-select-option',{attrs:{"value":"2"}},[_vm._v("لیسانس")]),_vm._v(" "),_c('b-select-option',{attrs:{"value":"3"}},[_vm._v("فوق لیسانس")]),_vm._v(" "),_c('b-select-option',{attrs:{"value":"4"}},[_vm._v("دکترا")])],1),_vm._v(" "),_c('label',{staticClass:"mt-4"},[_vm._v("شغل ")]),_vm._v(" "),_c('b-input',{class:{error : _vm.error.job},model:{value:(_vm.request.job),callback:function ($$v) {_vm.$set(_vm.request, "job", $$v)},expression:"request.job"}}),_vm._v(" "),_c('b-button',{staticClass:"btn-success w-100 mt-4 p-2",on:{"click":function($event){return _vm.validate3()}}},[_vm._v("\n          اتمام\n        ")])],1):_vm._e()],1)])],1):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/productSerial/_agentId/index.vue?vue&type=template&id=24a6d304&scoped=true&

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(7);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: ./plugins/Notification.js
var Notification = __webpack_require__(85);

// EXTERNAL MODULE: external "vue-select"
var external_vue_select_ = __webpack_require__(83);
var external_vue_select_default = /*#__PURE__*/__webpack_require__.n(external_vue_select_);

// EXTERNAL MODULE: ./node_modules/vue-select/dist/vue-select.css
var vue_select = __webpack_require__(101);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/productSerial/_agentId/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // import PDatePicker from 'vue2-persian-datepicker';



/* harmony default export */ var _agentIdvue_type_script_lang_js_ = ({
  name: "productSerial",

  async asyncData() {
    let xx = '';
    xx = {
      'name': 0,
      'phoneNumber': 0,
      'stateId': 0,
      'products': 0,
      'birthDate': 0,
      'MarriageDate': 0,
      'education': 0,
      'job': 0
    };
    return {
      score: xx
    };
  },

  data() {
    return {
      render: false,
      progress: '',
      closeBox: false,
      agentId: true,
      con: 1,
      level: 1,
      states: null,
      products: [],
      op: [{
        label: 'Canada',
        code: 'ca'
      }, {
        label: 'America',
        code: 'am'
      }],
      error: {
        name: null,
        phoneNumber: null,
        stateId: null,
        products: null,
        birthDate: null,
        MarriageDate: null,
        education: null,
        job: null
      },
      score: null,
      request: {
        name: "",
        agentId: this.$route.params.agentId,
        phoneNumber: "",
        stateId: "",
        products: [],
        birthDate: "",
        MarriageDate: "",
        education: "",
        job: ""
      }
    };
  },

  components: {
    vSelect: external_vue_select_default.a
  },
  computed: {
    scoreCon() {
      return this.score.name + this.score.phoneNumber + this.score.stateId + this.score.products + this.score.birthDate + this.score.MarriageDate + this.score.education + this.score.job;
    }

  },
  methods: {
    validate1(status) {
      let level = 0;

      if (this.request.name === "") {
        this.error.name = true;
        this.score.name = 0;
        level = 1;
      } else {
        this.error.name = false;
        this.score.name = 10;
      }

      if (this.request.phoneNumber === "" || this.request.phoneNumber.length !== 11) {
        this.error.phoneNumber = true;
        this.score.phoneNumber = 0;
        level = 1;
      } else {
        this.error.phoneNumber = false;
        this.score.phoneNumber = 10;
      }

      if (this.request.stateId === "") {
        this.error.stateId = true;
        this.score.stateId = 0;
        level = 1;
      } else {
        this.error.stateId = false;
        this.score.stateId = 10;
      }

      if (this.request.products.length === 0) {
        this.error.products = true;
        this.score.products = 0;
        level = 1;
      } else {
        this.error.products = false;
        this.score.products = 10;
      }

      if (level === 1) {
        return false;
      }

      if (status === 'end') {
        return this.newItem('اطلاعات شما با 40 امتیاز در قرعه کشی ثبت شد');
      }

      this.level = 2;
    },

    validate2(status) {
      // let level = true;
      // if (this.request.birthDate === "") {
      //     this.error.birthDate = true;
      //     this.score.birthDate = 0;
      //     level = false;
      // } else {
      //     this.error.birthDate = false;
      //     this.score.birthDate = 10
      // }
      //
      // if (this.request.MarriageDate === "") {
      //     this.error.MarriageDate = true;
      //     this.score.MarriageDate = 0;
      //     level = false;
      // } else {
      //     this.error.MarriageDate = false;
      //     this.score.MarriageDate = 10
      // }
      if (status === 'end') {
        return this.newItem('اطلاعت شما با 60 امتیاز در قرعه کشی ثبت شد');
      }

      this.level = 3;
    },

    validate3() {
      // let level = true;
      // if (this.request.education === "") {
      //     this.error.education = true;
      //     this.score.education = 0;
      //     level = false;
      // } else {
      //     this.error.education = false;
      //     this.score.education = 10
      // }
      //
      // if (this.request.job === "") {
      //     this.error.job = true;
      //     this.score.job = 0;
      //     level = false;
      // } else {
      //     this.error.job = false;
      //     this.score.job = 10
      // }
      return this.newItem('تمام امتیاز قرعه کشی را دریافت کردید.');
    },

    getState() {
      external_axios_default.a.get('/api/V1/serviceList/stateList').then(response => {
        this.states = response.data;
      }).catch(error => {
        console.log(error.response);
      });
    },

    getProducts() {
      external_axios_default.a.get('/api/V1/product/search/*').then(response => {
        this.products = response.data;
      }).catch(error => {
        console.log(error.response);
      });
    },

    checkAgent(id) {
      if (id === null) {
        this.agentId = false;
        this.request.agentId = null; // Notification.hideLoading();

        return 'false';
      }

      if (id.length !== 5) {
        this.agentId = false;
        this.request.agentId = null; // Notification.hideLoading();

        return 'false';
      }

      external_axios_default.a.post('/api/V1/productSerial/salesAgent/check', {
        'agentId': id
      }).then(response => {
        if (response.data === false) {
          this.agentId = false;
          this.request.agentId = null;
          return false;
        } else {
          this.agentId = true;
          return true;
        }
      }).catch(error => {
        console.log(error.response);
      });
    },

    async newItem(message) {
      // Notification.showLoading();
      await this.checkAgent(this.request.agentId);

      if (this.request.agentId == null || this.request.agentId === "") {
        // Notification.error('کد نمایندگی اشتباه است');
        return false;
      }

      external_axios_default.a.post('/api/V1/productSerial/customer/create', this.request).then(response => {
        this.closeBox = true;
        setTimeout(this.readier, 3); // Notification.success(' مشخصات شما با ' + this.scoreCon + ' امتیاز در قرعه کشی ثبت شد. ');
      }).catch(error => {
        console.log(error.response); // Notification.serverError(error.response.status);
      });
    },

    readier() {
      this.$router.push('/');
    }

  },

  created() {},

  mounted() {
    this.getState();
    this.getProducts();
    this.checkAgent(this.$route.params.agentId);
    this.render = true;
  }

});
// CONCATENATED MODULE: ./pages/productSerial/_agentId/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var productSerial_agentIdvue_type_script_lang_js_ = (_agentIdvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/productSerial/_agentId/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(228)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  productSerial_agentIdvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "24a6d304",
  "69db2688"
  
)

/* harmony default export */ var _agentId = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(toastr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(41);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__);


toastr__WEBPACK_IMPORTED_MODULE_0___default.a.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

class Notification {
  static success(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.success(msg);
  }

  static error(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error(msg);
  }

  static info(msg) {
    toastr__WEBPACK_IMPORTED_MODULE_0___default.a.info(msg);
  }

  static serverError(status) {
    if (status === 500) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.warning('ارتباط با سرور برقرار نشد.');
    }

    if (status === 401) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('برای دست رسی باید وارد شوید.');
    }

    if (status === 403) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('شما دسترسی لازم برای ورود به این بخش را ندارید.');
    }

    if (status === 404) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('صفحه مورد نظر یافت نشد.');
    }

    if (status === 422) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('اطلاعات به درستی وارد نشده است.');
    }
  }

  static showLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'block';
    }
  }

  static hideLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'none';
    }
  }

  static title(text) {
    document.title = text;
  }

  static description(text) {
    let descEl = document.querySelector('head meta[name="Description"]');
    descEl.setAttribute('content', text);
  }

}

/* harmony default export */ __webpack_exports__["a"] = (Notification);

/***/ })

};;
//# sourceMappingURL=index.js.map