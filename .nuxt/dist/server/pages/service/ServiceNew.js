exports.ids = [46];
exports.modules = {

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(219);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("42bbe03f", content, true, context)
};

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_ServiceNew_vue_vue_type_style_index_0_id_ff68e626_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(185);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_ServiceNew_vue_vue_type_style_index_0_id_ff68e626_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_ServiceNew_vue_vue_type_style_index_0_id_ff68e626_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_ServiceNew_vue_vue_type_style_index_0_id_ff68e626_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_ServiceNew_vue_vue_type_style_index_0_id_ff68e626_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".con2[data-v-ff68e626]{background-color:#f8f8f8}.title[data-v-ff68e626]{color:#707070}.value[data-v-ff68e626]{color:#00acd4;padding-right:7px}.con2[data-v-ff68e626]::-webkit-scrollbar{width:3px;background-color:hsla(0,0%,99.6%,.5)}.con2[data-v-ff68e626]::-webkit-scrollbar-thumb{background-color:rgba(0,172,212,.9);border-radius:6px}.con2[data-v-ff68e626]::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.34)}.input[data-v-ff68e626]::-webkit-scrollbar{width:3px;background-color:hsla(0,0%,99.6%,.5)}.input[data-v-ff68e626]::-webkit-scrollbar-thumb{background-color:rgba(0,172,212,.9);border-radius:6px}.input[data-v-ff68e626]::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(241,238,238,.5)}.label[data-v-ff68e626]{padding:7px 0 0!important}.input[data-v-ff68e626]{width:100%;padding:11px;border-radius:4px;border:1px solid #9f9b9b}.error[data-v-ff68e626]{border:2px solid #f56d6d!important}span[data-v-ff68e626]{font-size:11px;color:#f56d6d}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/service/ServiceNew.vue?vue&type=template&id=ff68e626&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"rounded border shadow mx-auto mt-4 ",staticStyle:{"max-width":"1600px"}},[_vm._ssrNode("<div class=\"con2  p-1 p-md-3\" data-v-ff68e626>","</div>",[_vm._ssrNode("<div data-v-ff68e626>","</div>",[_vm._ssrNode("<form onsubmit=\"event.preventDefault()\" class=\"row p-2 p-md-3 \" data-v-ff68e626>","</form>",[_vm._ssrNode("<div class=\"mb-4 col-12 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>\n              نمایندگی که می خواهید از آن خدمات بگیرید انتخاب کنید . اگر می خواد از دفتر مرکزی خدمات\n              بگیرید لطفا نمایندگی مرکزی را انتخاب کنید.\n            </p> "),_c('b-select',{class:['border overflow-hidden',{error:_vm.error.agentId}],attrs:{"name":"agent"},model:{value:(_vm.item.agentId),callback:function ($$v) {_vm.$set(_vm.item, "agentId", $$v)},expression:"item.agentId"}},_vm._l((_vm.agentList),function(agent,index){return _c('b-select-option',{key:index,attrs:{"value":agent.id,"selected":agent.name === 'دفتر مرکزی '}},[_vm._v("\n                "+_vm._s(agent.state)+" - "),_c('span',{staticClass:"font-weight-bold text-danger"},[_vm._v(_vm._s(agent.name))]),_vm._v(" -\n                "+_vm._s(agent.address)+"\n              ")])}),1)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\" mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12 \" data-v-ff68e626>\n              محصول که برای آن در خواست خدمات دارید انتخاب کنید.\n              <span data-v-ff68e626>"+_vm._ssrEscape(_vm._s(_vm.errorMessage.productId))+"</span></p> "),_c('b-input',{class:['input p-2 border col-12',{error : _vm.error.productId }],attrs:{"type":"search"},on:{"focus":function($event){_vm.setProduct = true},"keydown":function($event){return _vm.productSearch()}},model:{value:(_vm.productSearchInput),callback:function ($$v) {_vm.productSearchInput=$$v},expression:"productSearchInput"}}),_vm._ssrNode(" "+((_vm.setProduct)?("<div class=\"col-12 row\" style=\"position: absolute; z-index: 21; \" data-v-ff68e626><div class=\"input d-flex justify-content-between col-12 bg-white overflow-auto\" style=\"height: 300px; \" data-v-ff68e626><div class=\"col-11 p-0\" data-v-ff68e626>"+(_vm._ssrList((_vm.productSearchList),function(product,index){return ("<p class=\"p-2 col-12 border-bottom m-0\" style=\"font-size: 13px;cursor: pointer;\" data-v-ff68e626>"+_vm._ssrEscape("\n                    "+_vm._s(product.name)+" - "+_vm._s(product.model)+"\n                  ")+"</p>")}))+"</div> <i class=\"material-icons col-1 p-0\" style=\"color: #707070; text-align: left;cursor: pointer;\" data-v-ff68e626>\n                  close</i></div></div>"):"<!---->"))],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6\" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>\n              استان محل سوکونت خود را انتخاب کنید.\n            </p> "),_c('b-select',{class:['border col-12',{error:_vm.error.stateId}],attrs:{"name":"state"},model:{value:(_vm.item.stateId),callback:function ($$v) {_vm.$set(_vm.item, "stateId", $$v)},expression:"item.stateId"}},[_c('b-select-option',{attrs:{"value":""}}),_vm._v(" "),_vm._l((_vm.stateList),function(state,index){return _c('b-select-option',{key:index,attrs:{"value":state.id}},[_vm._v("\n                "+_vm._s(state.name)+"\n              ")])})],2)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>تاریخ شروع گارانتی</p> "),_vm._ssrNode("<div"+(_vm._ssrClass(null,['col-12 input border bg-white p-0 pt-1 pb-1',{error:_vm.error.serviceDate}]))+" data-v-ff68e626>","</div>",[_c('PDatePicker',{attrs:{"inputClass":"position-relative col-12  border-0 w-100 p-1 m-0 text-right","wrapperClass":"col-12 w-100 p-0"},model:{value:(_vm.item.serviceDate),callback:function ($$v) {_vm.$set(_vm.item, "serviceDate", $$v)},expression:"item.serviceDate"}})],1)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>سریال گارانتی</p> "),_c('b-input',{class:['input p-2 border col-12',{error:_vm.error.serialNumber}],attrs:{"name":"serialNumber","type":"text"},model:{value:(_vm.item.serialNumber),callback:function ($$v) {_vm.$set(_vm.item, "serialNumber", $$v)},expression:"item.serialNumber"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>نام و نام خانوادگی</p> "),_c('b-input',{class:['input border col-12',{error:_vm.error.name}],attrs:{"name":"name","type":"text"},model:{value:(_vm.item.name),callback:function ($$v) {_vm.$set(_vm.item, "name", $$v)},expression:"item.name"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>شماره تماس <span class=\"text-danger\" data-v-ff68e626>"+_vm._ssrEscape(_vm._s(_vm.errorMessage.phoneNumber))+"</span></p> "),_c('b-input',{class:['input border col-12',{error:_vm.error.phoneNumber}],attrs:{"name":"phoneNumber","type":"text"},model:{value:(_vm.item.phoneNumber),callback:function ($$v) {_vm.$set(_vm.item, "phoneNumber", $$v)},expression:"item.phoneNumber"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>آدرس <span data-v-ff68e626>"+_vm._ssrEscape(_vm._s(_vm.errorMessage.address))+"</span></p> "),_c('b-input',{class:['input border col-12',{error:_vm.error.address}],attrs:{"name":"address","type":"text"},model:{value:(_vm.item.address),callback:function ($$v) {_vm.$set(_vm.item, "address", $$v)},expression:"item.address"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 col-md-6 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-12\" data-v-ff68e626>کد پستی <span data-v-ff68e626>"+_vm._ssrEscape(_vm._s(_vm.errorMessage.postalCode))+"</span></p> "),_c('b-input',{class:['input border col-12',{error:_vm.error.postalCode}],attrs:{"name":"postalCode","type":"text"},model:{value:(_vm.item.postalCode),callback:function ($$v) {_vm.$set(_vm.item, "postalCode", $$v)},expression:"item.postalCode"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 \" data-v-ff68e626>","</div>",[_vm._ssrNode("<p class=\"label col-3\" data-v-ff68e626>توضیح</p> "),_c('b-textarea',{class:['input border col-12',{error:_vm.error.explanation}],attrs:{"rows":"7","name":"explanation","type":"text"},model:{value:(_vm.item.explanation),callback:function ($$v) {_vm.$set(_vm.item, "explanation", $$v)},expression:"item.explanation"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"form-group mb-4 col-12 \" data-v-ff68e626>","</div>",[_c('b-button',{staticClass:"form-group p-3 input btn btn-success col-12",on:{"click":function($event){return _vm.validate()}}},[_vm._v("ثبت")])],1)],2)])])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/service/ServiceNew.vue?vue&type=template&id=ff68e626&scoped=true&

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(7);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: ./plugins/Notification.js
var Notification = __webpack_require__(85);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/service/ServiceNew.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // import Comment from "../../../Services/commentCreate";
// import LoadingView from "../LoadingView";
// import PDatePicker from 'vue2-persian-datepicker';

/* harmony default export */ var ServiceNewvue_type_script_lang_js_ = ({
  name: "ServiceNew",

  data() {
    return {
      item: {
        productId: '',
        agentId: '',
        name: '',
        stateId: '',
        serviceDate: '',
        serialNumber: '',
        phoneNumber: '',
        address: '',
        postalCode: '',
        explanation: '',
        status: 1
      },
      error: {
        productId: false,
        agentId: false,
        name: false,
        stateId: false,
        serviceDate: false,
        serialNumber: false,
        phoneNumber: false,
        address: false,
        postalCode: false,
        explanation: false
      },
      errorMessage: {
        productId: '',
        agentId: '',
        name: '',
        stateId: '',
        serviceDate: '',
        serialNumber: '',
        phoneNumber: '',
        address: '',
        postalCode: '',
        explanation: ''
      },
      comment: {
        text: 'در خواست خدمات توسط کاربر ثبت شد.',
        view: 1,
        serviceId: '',
        userStatus: 1,
        api_token: ''
      },
      statusList: null,
      agentList: null,
      stateList: null,
      productList: null,
      productSearchList: [],
      productSearchInput: null,
      setProduct: false
    };
  },

  components: {// PDatePicker,
  },
  methods: {
    close() {
      this.$emit('closeOpenBox');
    },

    validate() {
      if (this.item.agentId === '') {
        this.error.agentId = true;
      } else {
        this.error.agentId = false;
      }

      if (this.item.productId === '') {
        this.error.productId = true;
        this.errorMessage.productId = 'محصول را از لیست انتخاب کنید';
      } else {
        this.error.productId = false;
        this.errorMessage.productId = '';
      }

      if (this.item.name === '') {
        this.error.name = true;
      } else {
        this.error.name = false;
      }

      if (this.item.stateId === '') {
        this.error.stateId = true;
      } else {
        this.error.stateId = false;
      }

      if (this.item.phoneNumber === '') {
        this.error.phoneNumber = true;
      } else {
        if (!this.item.phoneNumber.match(/^[0-9]+$/)) {
          this.error.phoneNumber = true;
          this.errorMessage.phoneNumber = 'شماره تماس را با اعداد انگلیسی وارد کنید';
        } else {
          if (this.item.phoneNumber.length === 11) {
            this.error.phoneNumber = false;
            this.errorMessage.phoneNumber = '';
          } else {
            this.error.phoneNumber = true;
            this.errorMessage.phoneNumber = 'شماره تماس باید 11 عدد باشد';
          }
        }
      }

      if (this.item.address === '') {
        this.error.address = true;
        this.errorMessage.address = 'در صورت نیاز به ارسال قطعه ورود آدرس الزامی است';
      } else {
        this.error.address = false;
        this.errorMessage.address = '';
      }

      if (this.item.postalCode === '') {
        this.error.postalCode = true;
        this.errorMessage.postalCode = 'در صورت نیاز به ارسال قطعه ورود کد پستی الزامی است';
      } else {
        this.error.postalCode = false;
        this.errorMessage.postalCode = '';
      }

      if (this.item.explanation === '') {
        this.error.explanation = true;
      } else {
        this.error.explanation = false;
      }

      if (this.error.agentId || this.error.productId || this.error.stateId || this.error.name || this.error.phoneNumber || this.error.explanation) {
        return false;
      } else {
        this.create();
      }
    },

    getAgent() {
      external_axios_default.a.get('/api/V1/serviceAgent/*').then(response => {
        // console.log(response);
        this.agentList = response.data.data;
      }).catch(response => {// console.log(response.error);
      });
    },

    getProduct() {
      external_axios_default.a.get('/api/V1/product/search/*').then(response => {
        // console.log(response);
        this.productList = response.data.data;
        this.productSearchList = response.data.data;
      }).catch(response => {// console.log(response.error);
      });
    },

    getState() {
      external_axios_default.a.get('/api/V1/serviceList/stateList').then(response => {
        // console.log(response);
        this.stateList = response.data;
      }).catch(response => {// console.log(response.error);
      });
    },

    productSearch() {
      if (this.productSearchInput === "") {
        return this.productSearchList = this.productList;
      } else {
        this.productSearchList = this.productList.filter(item => {
          let input = this.productSearchInput.split(" ");

          for (let i = 0; i <= input.length; i++) {
            let name = item.name.indexOf(input[i]);
            let model = item.model.indexOf(input[i]);

            if (name !== -1 || model !== -1) {
              return item;
            }
          }
        });
      }
    },

    getStatus() {
      external_axios_default.a.get('/api/V1/serviceList/statusList/1').then(response => {
        this.item.status = response.data.data[0].id;
        console.log(response);
      }).catch(error => {
        console.log(error.response);
      });
    },

    async create() {
      Notification["a" /* default */].showLoading();
      await external_axios_default.a.post('/api/V1/serviceList/create', this.item).then(response => {
        console.log(response);
        Notification["a" /* default */].success('درخواست جدید ثبت شد');
        this.$emit('addNewItem', response.data);
        this.comment.serviceId = response.data.id;
        Comment.create(this.comment);
        this.$router.push('/home/serviceOpen/' + response.data.numberFollow);
      }).catch(error => {
        Notification["a" /* default */].serverError(error.response.status);
        console.log(error.response);
      });
    }

  },

  created() {
    this.getAgent();
    this.getProduct();
    this.getState();
    this.getStatus();
  }

});
// CONCATENATED MODULE: ./pages/service/ServiceNew.vue?vue&type=script&lang=js&
 /* harmony default export */ var service_ServiceNewvue_type_script_lang_js_ = (ServiceNewvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/service/ServiceNew.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(218)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  service_ServiceNewvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "ff68e626",
  "ee8cba44"
  
)

/* harmony default export */ var ServiceNew = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(toastr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(41);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__);


toastr__WEBPACK_IMPORTED_MODULE_0___default.a.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

class Notification {
  static success(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.success(msg);
  }

  static error(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error(msg);
  }

  static info(msg) {
    toastr__WEBPACK_IMPORTED_MODULE_0___default.a.info(msg);
  }

  static serverError(status) {
    if (status === 500) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.warning('ارتباط با سرور برقرار نشد.');
    }

    if (status === 401) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('برای دست رسی باید وارد شوید.');
    }

    if (status === 403) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('شما دسترسی لازم برای ورود به این بخش را ندارید.');
    }

    if (status === 404) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('صفحه مورد نظر یافت نشد.');
    }

    if (status === 422) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('اطلاعات به درستی وارد نشده است.');
    }
  }

  static showLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'block';
    }
  }

  static hideLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'none';
    }
  }

  static title(text) {
    document.title = text;
  }

  static description(text) {
    let descEl = document.querySelector('head meta[name="Description"]');
    descEl.setAttribute('content', text);
  }

}

/* harmony default export */ __webpack_exports__["a"] = (Notification);

/***/ })

};;
//# sourceMappingURL=ServiceNew.js.map