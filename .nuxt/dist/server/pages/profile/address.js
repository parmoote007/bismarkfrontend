exports.ids = [43,4,25];
exports.modules = {

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/profile/profileMenu.vue?vue&type=template&id=7130eec3&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"border rounded shadow p-3"},[_c('nuxt-link',{attrs:{"to":"/profile"}},[_c('div',{staticClass:"d-flex align-items-center pb-3 border-bottom"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"60px","color":"#dddddd"}},[_vm._v("account_circle")]),_vm._v(" "),_c('div',{staticClass:"mr-3  "},[_c('p',{staticClass:"p-0 m-0"},[_vm._v("\n          "+_vm._s(_vm.user.name)+"\n        ")]),_vm._v(" "),_c('p',{staticClass:"pt-1 m-0",staticStyle:{"font-size":"12px","color":"#808080"}},[_vm._v(_vm._s(_vm.user.phoneNumber))])])])]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"menu mt-3 border-bottom\" data-v-7130eec3>","</div>",[_c('nuxt-link',{attrs:{"to":"/card/cardList"}},[_c('div',{staticClass:"d-flex  p-2"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"29px"}},[_vm._v("list_alt")]),_vm._v(" "),_c('p',{staticClass:"pr-3 pt-1 font-weight-bold",staticStyle:{"font-size":"13px","color":"#646363"}},[_vm._v("سفارش های من")])])]),_vm._ssrNode(" <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>favorite_border</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>مورد علاقه ها</p></div> <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>chat_bubble_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>نظرات</p></div> "),_c('nuxt-link',{attrs:{"to":"/profile/address"}},[_c('div',{staticClass:"d-flex  p-2"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"29px"}},[_vm._v("house_siding")]),_vm._v(" "),_c('p',{staticClass:"pr-3 pt-1 font-weight-bold",staticStyle:{"font-size":"13px","color":"#646363"}},[_vm._v("آدرس")])])]),_vm._ssrNode(" <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>mail_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>پیغام ها</p></div> <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>person_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3> اطلاعات حساب</p></div>")],2),_vm._ssrNode(" <div class=\"d-flex  pt-4 pr-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>logout</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>خروج</p></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/profile/profileMenu.vue?vue&type=template&id=7130eec3&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/profile/profileMenu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var profileMenuvue_type_script_lang_js_ = ({
  name: "profileMenu",
  computed: {
    user() {
      return this.$store.state.userInfo;
    }

  }
});
// CONCATENATED MODULE: ./components/profile/profileMenu.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_profileMenuvue_type_script_lang_js_ = (profileMenuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/profile/profileMenu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(115)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_profileMenuvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7130eec3",
  "39d67d23"
  
)

/* harmony default export */ var profileMenu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(97);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 116:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".menu p[data-v-7130eec3]:hover{color:#3b8070}.menu i[data-v-7130eec3],.menu p[data-v-7130eec3]{cursor:pointer}a[data-v-7130eec3]{text-decoration:none;color:#646363!important}a[data-v-7130eec3]:hover,a:hover p[data-v-7130eec3]{text-decoration:none;color:#0fabc6!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(217);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("f291a8ba", content, true, context)
};

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_address_vue_vue_type_style_index_0_id_129b4c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(184);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_address_vue_vue_type_style_index_0_id_129b4c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_address_vue_vue_type_style_index_0_id_129b4c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_address_vue_vue_type_style_index_0_id_129b4c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_address_vue_vue_type_style_index_0_id_129b4c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "input[data-v-129b4c68]{padding:18px 10px}input[data-v-129b4c68],textarea[data-v-129b4c68]{font-size:13px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/profile/address.vue?vue&type=template&id=129b4c68&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"mx-auto ",staticStyle:{"max-width":"1600px"}},[_vm._ssrNode("<div class=\"d-flex mt-4 mt-md-5 mb-4 overflow-hidden\" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"mr-3 d-none d-md-block  \" style=\"max-width: 320px; width: 30%\" data-v-129b4c68>","</div>",[_c('ProfileMenu')],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"flex-grow-1  mx-3 mx-md-4\" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\" \" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"border shadow rounded overflow-hidden\" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"p-1  p-md-4\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p class=\"font-weight-bold text-dark p-2 p-md-0\" data-v-129b4c68>تکمیل اطلاعات سفارش</p> <p class=\"px-2 px-md-0\" style=\"font-size: 12px; font-weight: bold; color: #808080\" data-v-129b4c68>اطلاعات ضروری جهت ثبت سفارش شما</p> "+(_vm._ssrList((_vm.errors),function(e,index){return ("<p data-v-129b4c68>"+_vm._ssrEscape(_vm._s(e))+"</p>")}))+" "),_vm._ssrNode("<div class=\"d-md-flex mt-4 \" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"col-12 col-md-5\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>استان</p> "),_c('b-select',{model:{value:(_vm.address.stateId),callback:function ($$v) {_vm.$set(_vm.address, "stateId", $$v)},expression:"address.stateId"}},_vm._l((_vm.states),function(s,index){return _c('b-select-option',{key:index,attrs:{"value":s.id}},[_vm._v(_vm._s(s.name))])}),1)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 col-md-5 mt-4 mt-md-0\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>شهرستان</p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"شهرستان محل سکونت خود را وارد کنید"},model:{value:(_vm.address.city),callback:function ($$v) {_vm.$set(_vm.address, "city", $$v)},expression:"address.city"}})],2)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"mt-4 col-12 col-md-10\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>آدرس</p> "),_c('b-textarea',{staticClass:"p-2",attrs:{"placeholder":"ادرس را وارد کنید ..."},model:{value:(_vm.address.address),callback:function ($$v) {_vm.$set(_vm.address, "address", $$v)},expression:"address.address"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"mt-4 col-12 col-md-10\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>کد پستی</p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"کد پستی را وارد کنید .."},model:{value:(_vm.address.postalCode),callback:function ($$v) {_vm.$set(_vm.address, "postalCode", $$v)},expression:"address.postalCode"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"d-md-flex mt-4\" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"col-12 col-md-5\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>نام تحویل گیرنده</p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"نام تحویل گیرنده .."},model:{value:(_vm.address.name),callback:function ($$v) {_vm.$set(_vm.address, "name", $$v)},expression:"address.name"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 col-md-5 mt-4 mt-md-0\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>نام خانوادگی تحویل گیرنده</p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"نام خانوادگی تحویل گیرنده .."},model:{value:(_vm.address.family),callback:function ($$v) {_vm.$set(_vm.address, "family", $$v)},expression:"address.family"}})],2)],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"d-flex mt-4\" data-v-129b4c68>","</div>",[_vm._ssrNode("<div class=\"col-6 col-md-5\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>شماره موبایل</p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"شماره تماس .."},model:{value:(_vm.address.phoneNumber),callback:function ($$v) {_vm.$set(_vm.address, "phoneNumber", $$v)},expression:"address.phoneNumber"}})],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6 col-md-5\" data-v-129b4c68>","</div>",[_vm._ssrNode("<p data-v-129b4c68>کد ملی </p> "),_c('b-form-input',{attrs:{"type":"text","placeholder":"کد ملی"},model:{value:(_vm.address.nationalCode),callback:function ($$v) {_vm.$set(_vm.address, "nationalCode", $$v)},expression:"address.nationalCode"}})],2)],2)],2),_vm._ssrNode(" <div class=\"d-block d-md-flex overflow-hidden align-items-center justify-content-between font-weight-bold p-3 mt-4 border-top\" style=\"background-color: #f8fbfc\" data-v-129b4c68><div class=\"col-12 pt-3 d-flex justify-content-center\" data-v-129b4c68><p class=\"px-5 py-3 rounded text-white text-center font-weight-bold\" style=\"background-color: #2164e1; cursor: pointer \" data-v-129b4c68>\n                ذخیره\n              </p></div></div>")],2)])])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/profile/address.vue?vue&type=template&id=129b4c68&scoped=true&

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(7);

// EXTERNAL MODULE: ./plugins/Notification.js
var Notification = __webpack_require__(85);

// EXTERNAL MODULE: ./components/card/cardBack.vue + 4 modules
var cardBack = __webpack_require__(98);

// EXTERNAL MODULE: ./components/profile/profileMenu.vue + 4 modules
var profileMenu = __webpack_require__(105);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./pages/profile/address.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ var addressvue_type_script_lang_js_ = ({
  name: "address",

  fetch() {
    return this.$axios.$get('/api/V1/serviceList/stateList').then(response => {
      // console.log(response)
      this.states = response;
    }).catch(error => {
      console.log(error.response);
    });
  },

  data() {
    return {
      loading: false,
      states: [],
      stateId: null,
      address: {
        name: '',
        family: '',
        stateId: null,
        city: '',
        address: '',
        postalCode: '',
        phoneNumber: '',
        nationalCode: ''
      },
      errors: []
    };
  },

  watch: {},
  components: {
    ProfileMenu: profileMenu["default"],
    CardBack: cardBack["default"]
  },
  computed: {},
  methods: {
    validate() {
      if (this.address.name === '' || this.address.family === '' || this.address.city === '' || this.address.stateId === '' || this.address.nationalCode === '' || this.address.postalCode === '' || this.address.address === '') {
        this.$store.commit('EDIT_CARD_ADDRESS_STATUS', false);
        return Notification["a" /* default */].error('اطلاعات را بصورت کامل وارد کنید');
      }

      if (this.address.phoneNumber.length !== 11) {
        Notification["a" /* default */].error('شماره تماس باید 11 عدد باشد');
        this.$store.commit('EDIT_CARD_ADDRESS_STATUS', false);
        return false;
      }

      this.createAddress();
    },

    getAddress() {
      this.$axios.$get('/api/V1/card/customer/address/get/' + localStorage.getItem('api_token')).then(response => {
        if (response.data !== null) {
          this.address = response;
          this.stateId = response.stateId;
        }

        console.log(response);
      }).catch(error => {
        console.log(error.response);
      });
    },

    async createAddress() {
      this.address.api_token = this.$store.state.user.api_token;
      await this.$axios.$post('/api/V1/card/customer/address/create', this.address).then(response => {
        this.$store.commit('EDIT_CARD_ADDRESS_STATUS', true);
        console.log(response);
      }).catch(error => {
        console.log(error.response);
        this.errors = error.response.data.errors;
        Notification["a" /* default */].serverError(error.response.status);
      });
    }

  },

  mounted() {
    this.getAddress();
  }

});
// CONCATENATED MODULE: ./pages/profile/address.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_addressvue_type_script_lang_js_ = (addressvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/profile/address.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(216)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_addressvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "129b4c68",
  "8b8cf736"
  
)

/* harmony default export */ var address = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ProfileMenu: __webpack_require__(105).default})


/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(toastr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(41);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__);


toastr__WEBPACK_IMPORTED_MODULE_0___default.a.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

class Notification {
  static success(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.success(msg);
  }

  static error(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error(msg);
  }

  static info(msg) {
    toastr__WEBPACK_IMPORTED_MODULE_0___default.a.info(msg);
  }

  static serverError(status) {
    if (status === 500) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.warning('ارتباط با سرور برقرار نشد.');
    }

    if (status === 401) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('برای دست رسی باید وارد شوید.');
    }

    if (status === 403) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('شما دسترسی لازم برای ورود به این بخش را ندارید.');
    }

    if (status === 404) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('صفحه مورد نظر یافت نشد.');
    }

    if (status === 422) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('اطلاعات به درستی وارد نشده است.');
    }
  }

  static showLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'block';
    }
  }

  static hideLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'none';
    }
  }

  static title(text) {
    document.title = text;
  }

  static description(text) {
    let descEl = document.querySelector('head meta[name="Description"]');
    descEl.setAttribute('content', text);
  }

}

/* harmony default export */ __webpack_exports__["a"] = (Notification);

/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(116);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("571f3da1", content, true, context)
};

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/cardBack.vue?vue&type=template&id=3a835fd0&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<i class=\"material-icons d-sm-none text-white font-weight-bold rounded px-2 py-1 m-1\" style=\"background-color: #377dff \" data-v-3a835fd0>\n    east\n  </i>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card/cardBack.vue?vue&type=template&id=3a835fd0&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/cardBack.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var cardBackvue_type_script_lang_js_ = ({
  name: "cardBack",
  props: ['link'],
  methods: {
    go() {
      this.$router.push(this.link);
    }

  }
});
// CONCATENATED MODULE: ./components/card/cardBack.vue?vue&type=script&lang=js&
 /* harmony default export */ var card_cardBackvue_type_script_lang_js_ = (cardBackvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card/cardBack.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  card_cardBackvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3a835fd0",
  "109665de"
  
)

/* harmony default export */ var cardBack = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=address.js.map