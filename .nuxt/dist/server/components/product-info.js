exports.ids = [13];
exports.modules = {

/***/ 139:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(153);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("1d8f9347", content, true, context)
};

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_info_vue_vue_type_style_index_0_id_491889f2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(139);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_info_vue_vue_type_style_index_0_id_491889f2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_info_vue_vue_type_style_index_0_id_491889f2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_info_vue_vue_type_style_index_0_id_491889f2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_info_vue_vue_type_style_index_0_id_491889f2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "@media only screen and (max-width:768px){*[data-v-491889f2]{font-size:12px!important}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/info.vue?vue&type=template&id=491889f2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode(((_vm.items === null)?("<div data-v-491889f2></div>"):"<!---->")+" "+((_vm.explanation !== null)?("<div data-v-491889f2><p class=\"px-3 py-2 font-weight-bold\" style=\"font-size: 15px\" data-v-491889f2><span class=\"pb-1\" data-v-491889f2>توضیح محصول : </span></p> <div class=\"px-3 pb-1\" style=\"font-size: 13px;line-height: 28px; text-align: justify\" data-v-491889f2>"+(_vm._s(_vm.explanation))+"</div> <p class=\"px-3 py-1 font-weight-bold\" style=\"font-size: 15px\" data-v-491889f2><span data-v-491889f2>مشخصات محصول : </span></p></div>"):"<!---->")+" "+((_vm.items !=null)?("<div class=\"mt-2 rounded\" style=\"font-size: 13px\" data-v-491889f2>"+(_vm._ssrList((_vm.items),function(item,index){return ("<div class=\"d-flex\" data-v-491889f2><div class=\"col-4 col-md-3 p-0 m-0\" data-v-491889f2><div class=\" my-2 mr-2 ml-0 p-3 \" style=\"background-color:#fafafa\" data-v-491889f2>"+_vm._ssrEscape("\n          "+_vm._s(item.sp)+" :\n        ")+"</div></div> <div class=\"col-8 col-md-9 p-0 m-0\" data-v-491889f2><div class=\"my-2 mr-4 ml-2 p-3 \" style=\"background-color:#fafafa\" data-v-491889f2>"+_vm._ssrEscape("\n          "+_vm._s(item.value)+"\n        ")+"</div></div></div>")}))+"</div>"):"<!---->"))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/product/info.vue?vue&type=template&id=491889f2&scoped=true&

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(7);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/info.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import { VueLoading } from 'vue-loading-template';

/* harmony default export */ var infovue_type_script_lang_js_ = ({
  props: ['id', 'explanation'],
  name: "productInfo",

  fetch() {
    return this.$axios.$get('/api/V1/productSpecificationValue/product/' + this.id).then(response => {
      // console.log(response);
      this.items = response.data;
    }).catch(error => {// console.log(error.response);
    });
  },

  data() {
    return {
      items: null
    };
  },

  watch: {
    id: function () {
      this.getData();
    }
  },
  components: {// VueLoading
  },
  methods: {
    getData() {
      this.items = null;
      this.$axios.$get('/api/V1/productSpecificationValue/product/' + this.id).then(response => {
        console.log(response);
        this.items = response.data;
      }).catch(error => {
        console.log(error.response);
      });
    }

  },

  mounted() {// this.getData();
  }

});
// CONCATENATED MODULE: ./components/product/info.vue?vue&type=script&lang=js&
 /* harmony default export */ var product_infovue_type_script_lang_js_ = (infovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/product/info.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(152)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  product_infovue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "491889f2",
  "7078c586"
  
)

/* harmony default export */ var info = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=product-info.js.map