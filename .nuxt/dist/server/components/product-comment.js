exports.ids = [12];
exports.modules = {

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productComment.vue?vue&type=template&id=cf33c9e8&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{},[_vm._ssrNode("<div class=\"p-4\" data-v-cf33c9e8>","</div>",[_vm._ssrNode("<h5 class=\"pb-2 font-weight-bold\" style=\"font-size: 15px\" data-v-cf33c9e8>ثبت نظر جدید</h5> "),_c('b-textarea',{attrs:{"rows":"7","placeholder":"نظر خود را وارد کنید ..."},model:{value:(_vm.newItem.text),callback:function ($$v) {_vm.$set(_vm.newItem, "text", $$v)},expression:"newItem.text"}}),_vm._ssrNode(" "),_c('b-button',{staticClass:"mt-3 py-2 px-4",staticStyle:{"font-size":"14px","font-weight":"bold","background-color":"#377dff"},on:{"click":_vm.create}},[_vm._v("ارسال نظر")])],2),_vm._ssrNode(" "+(_vm._ssrList((_vm.comments),function(comment,index){return ("<div class=\"d-md-flex pt-4 pb-4 m-4 border-bottom \" data-v-cf33c9e8><div class=\"d-flex d-md-block cdl-12 col-md-3 pl-4 \" data-v-cf33c9e8><div class=\"d-flex\" style=\"color: #5b5959\" data-v-cf33c9e8><i class=\"material-icons\" data-v-cf33c9e8>person</i> <p class=\"p-1\" data-v-cf33c9e8>"+_vm._ssrEscape(" "+_vm._s(comment.user)+" ")+"</p></div> <div class=\"d-flex \" style=\"color: #7e7d7d;font-size: 13px\" data-v-cf33c9e8><i class=\"material-icons pr-2 pr-md-0\" style=\"font-size: 16px; padding-top: 5px\" data-v-cf33c9e8>date_range</i> <p class=\"p-1\" data-v-cf33c9e8>"+_vm._ssrEscape(" "+_vm._s(comment.date))+"</p></div></div> <div class=\"pr-4 border-right\" data-v-cf33c9e8><p style=\"color: #707070\" data-v-cf33c9e8>"+_vm._ssrEscape(" "+_vm._s(comment.text)+" ")+"</p></div></div>")})))],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/product/productComment.vue?vue&type=template&id=cf33c9e8&scoped=true&

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(7);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: ./plugins/Notification.js
var Notification = __webpack_require__(85);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productComment.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // import {VueLoading} from 'vue-loading-template';

/* harmony default export */ var productCommentvue_type_script_lang_js_ = ({
  name: "productComment",
  props: ['id'],

  data() {
    return {
      error: null,
      comments: null,
      newItem: {
        'text': '',
        'productId': this.id,
        'api_token': null
      }
    };
  },

  components: {},
  watch: {
    id: function () {
      this.getComment();
    }
  },
  methods: {
    getComment() {
      external_axios_default.a.get('/api/V1/comment/product/' + this.id).then(response => {
        this.comments = response.data;
        console.log(response);
      }).catch(error => {
        Notification["a" /* default */].serverError(error.response.status);
        console.log(error.response);
      });
    },

    create() {
      this.newItem.api_token = localStorage.getItem('api_token');

      if (this.newItem.api_token === null) {
        return Notification["a" /* default */].error('برای ثبت نظر باید وارد شوید.');
      }

      if (this.newItem.text === '') {
        return Notification["a" /* default */].error('متن نظر خالی است.');
      }

      external_axios_default.a.post('/api/V1/comment/product/create', this.newItem).then(response => {
        console.log(response);
        Notification["a" /* default */].success('نظر شما ثبت شد.');
        this.newItem.text = '';
        this.getComment();
      }).catch(error => {
        console.log(error.response);
        this.error = error.response;
        Notification["a" /* default */].serverError(error.response.status);
      });
    }

  },

  created() {
    this.getComment();
  },

  updated() {
    this.newItem.productId = this.id;
  }

});
// CONCATENATED MODULE: ./components/product/productComment.vue?vue&type=script&lang=js&
 /* harmony default export */ var product_productCommentvue_type_script_lang_js_ = (productCommentvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/product/productComment.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  product_productCommentvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "cf33c9e8",
  "a7ddc582"
  
)

/* harmony default export */ var productComment = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var toastr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(toastr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(41);
/* harmony import */ var toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(toastr_build_toastr_min_css__WEBPACK_IMPORTED_MODULE_1__);


toastr__WEBPACK_IMPORTED_MODULE_0___default.a.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

class Notification {
  static success(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.success(msg);
  }

  static error(msg) {
    this.hideLoading();
    return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error(msg);
  }

  static info(msg) {
    toastr__WEBPACK_IMPORTED_MODULE_0___default.a.info(msg);
  }

  static serverError(status) {
    if (status === 500) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.warning('ارتباط با سرور برقرار نشد.');
    }

    if (status === 401) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('برای دست رسی باید وارد شوید.');
    }

    if (status === 403) {
      this.hideLoading();
      toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('شما دسترسی لازم برای ورود به این بخش را ندارید.');
    }

    if (status === 404) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('صفحه مورد نظر یافت نشد.');
    }

    if (status === 422) {
      this.hideLoading();
      return toastr__WEBPACK_IMPORTED_MODULE_0___default.a.error('اطلاعات به درستی وارد نشده است.');
    }
  }

  static showLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'block';
    }
  }

  static hideLoading() {
    if (document.getElementById('loading')) {
      document.getElementById('loading').style.display = 'none';
    }
  }

  static title(text) {
    document.title = text;
  }

  static description(text) {
    let descEl = document.querySelector('head meta[name="Description"]');
    descEl.setAttribute('content', text);
  }

}

/* harmony default export */ __webpack_exports__["a"] = (Notification);

/***/ })

};;
//# sourceMappingURL=product-comment.js.map