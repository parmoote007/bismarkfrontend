exports.ids = [11];
exports.modules = {

/***/ 142:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(165);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("6e579c5a", content, true, context)
};

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/scale.e2f2dc4.jpg";

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/s4.eb37162.jpg";

/***/ }),

/***/ 163:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/s2.2194830.jpg";

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_HomeBanner_vue_vue_type_style_index_0_id_59c83e28_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(142);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_HomeBanner_vue_vue_type_style_index_0_id_59c83e28_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_HomeBanner_vue_vue_type_style_index_0_id_59c83e28_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_HomeBanner_vue_vue_type_style_index_0_id_59c83e28_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_HomeBanner_vue_vue_type_style_index_0_id_59c83e28_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".rounded[data-v-59c83e28]{border-radius:10px!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/HomeBanner.vue?vue&type=template&id=59c83e28&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"d-flex justify-content-center my-4 mx-auto",staticStyle:{"max-width":"1600px"}},[_vm._ssrNode("<div class=\"col-12 col-xl-11 d-flex justify-content-center align-content-between  p-0 m-0 \" data-v-59c83e28>","</div>",[_vm._ssrNode("<div class=\"col-12 col-xl-8  rounded m-0  ml-xl-3 pb-0 mt-n3 mt-xl-0 px-md-4 p-xl-0  \" data-v-59c83e28>","</div>",[_vm._ssrNode("<div class=\"rounded\" data-v-59c83e28>","</div>",[_c('b-carousel',{staticClass:"m-0 p-0 rounded",staticStyle:{"text-shadow":"1px 1px 2px #333"},attrs:{"id":"carousel-1","interval":4000,"background":"#ababab"},on:{"sliding-start":_vm.onSlideStart,"sliding-end":_vm.onSlideEnd},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},[_c('b-carousel-slide',{staticClass:"rounded p-0 m-0 ",scopedSlots:_vm._u([{key:"img",fn:function(){return [_c('img',{directives:[{name:"lazy-load",rawName:"v-lazy-load"}],staticClass:"w-100 rounded shadow-sm",attrs:{"data-src":__webpack_require__(161),"alt":""}})]},proxy:true}])})],1)],1)]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"flex-grow-1 d-none d-xl-block\" data-v-59c83e28>","</div>",[_vm._ssrNode("<div class=\"   p-0  m-0  rounded\" data-v-59c83e28>","</div>",[_vm._ssrNode("<div data-v-59c83e28>","</div>",[_c('router-link',{attrs:{"to":"/category/کمک-آشپز"}},[_c('img',{directives:[{name:"lazy-load",rawName:"v-lazy-load"}],staticClass:"w-100 rounded shadow-sm",attrs:{"data-src":__webpack_require__(162),"alt":""}})])],1)]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"p-0 pt-3  m-0 rounded\" data-v-59c83e28>","</div>",[_vm._ssrNode("<div data-v-59c83e28>","</div>",[_c('router-link',{attrs:{"to":"/category/تهیه-نوشیدنی"}},[_c('img',{directives:[{name:"lazy-load",rawName:"v-lazy-load"}],staticClass:"w-100 rounded shadow-sm",attrs:{"data-src":__webpack_require__(163),"alt":""}})])],1)])],2)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/HomeBanner.vue?vue&type=template&id=59c83e28&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/HomeBanner.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var HomeBannervue_type_script_lang_js_ = ({
  name: "HomeBanner",

  // fetch() {
  //   // return this.slidList =  this.$axios.$get('/api/V1/setting/banner/listView');
  //   return this.$axios.$get('/api/V1/setting/banner/listView')
  //     .then(response => {
  //       console.log(response)
  //       this.slidList = response;
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     })
  // },
  data() {
    return {
      slide: 0,
      slidList: [],
      sliding: null
    };
  },

  methods: {
    onSlideStart(slide) {
      this.sliding = true;
    },

    onSlideEnd(slide) {
      this.sliding = false;
    }

  },

  mounted() {}

});
// CONCATENATED MODULE: ./components/HomeBanner.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_HomeBannervue_type_script_lang_js_ = (HomeBannervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/HomeBanner.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(164)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_HomeBannervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "59c83e28",
  "511c894d"
  
)

/* harmony default export */ var HomeBanner = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=home-banner.js.map