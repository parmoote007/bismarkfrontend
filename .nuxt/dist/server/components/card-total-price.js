exports.ids = [7];
exports.modules = {

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/totalPrice.vue?vue&type=template&id=179c61e4&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<div class=\"border rounded overflow-hidden\" data-v-179c61e4><div class=\" font-weight-bold p-3 \" data-v-179c61e4><div data-v-179c61e4><div class=\"d-flex justify-content-between mt-1\" data-v-179c61e4><p style=\"color: #808080\" data-v-179c61e4>جمع سبد خرید </p> <p class=\"mr-2\" style=\"font-size: 15px\" data-v-179c61e4>"+_vm._ssrEscape("\n              "+_vm._s(_vm._f("formatNumber")(_vm.cardTotalPrice))+"\n              ")+"<span style=\"font-weight: 500\" data-v-179c61e4> تومان </span></p></div> "+((_vm.discount !== null)?("<div class=\"d-flex justify-content-between mt-1\" data-v-179c61e4><p style=\"color: #808080\" data-v-179c61e4>تخفیف </p> <p class=\"mr-2\" style=\"font-size: 15px\" data-v-179c61e4>"+_vm._ssrEscape("\n              "+_vm._s(_vm._f("formatNumber")((_vm.cardTotalPrice) * (_vm.discount / 100)))+"\n              ")+"<span style=\"font-weight: 500\" data-v-179c61e4> تومان </span></p></div>"):"<!---->")+" "+((_vm.discount !== null)?("<div class=\"d-flex justify-content-between mt-1\" data-v-179c61e4><p style=\"color: #808080\" data-v-179c61e4>قابل پرداخت </p> <p class=\"mr-2\" style=\"font-size: 15px\" data-v-179c61e4>"+_vm._ssrEscape("\n              "+_vm._s(_vm._f("formatNumber")((_vm.cardTotalPrice) * ((100 - _vm.discount) / 100)))+"\n              ")+"<span style=\"font-weight: 500\" data-v-179c61e4> تومان </span></p></div>"):"<!---->")+"</div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card/totalPrice.vue?vue&type=template&id=179c61e4&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/totalPrice.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var totalPricevue_type_script_lang_js_ = ({
  name: "totalPrice",
  computed: {
    cardTotalPrice() {
      return this.$store.state.cardTotal;
    },

    discount() {
      return this.$store.state.discountCode;
    }

  }
});
// CONCATENATED MODULE: ./components/card/totalPrice.vue?vue&type=script&lang=js&
 /* harmony default export */ var card_totalPricevue_type_script_lang_js_ = (totalPricevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card/totalPrice.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  card_totalPricevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "179c61e4",
  "7a9769df"
  
)

/* harmony default export */ var totalPrice = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=card-total-price.js.map