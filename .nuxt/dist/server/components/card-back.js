exports.ids = [4];
exports.modules = {

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/cardBack.vue?vue&type=template&id=3a835fd0&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<i class=\"material-icons d-sm-none text-white font-weight-bold rounded px-2 py-1 m-1\" style=\"background-color: #377dff \" data-v-3a835fd0>\n    east\n  </i>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card/cardBack.vue?vue&type=template&id=3a835fd0&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/card/cardBack.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var cardBackvue_type_script_lang_js_ = ({
  name: "cardBack",
  props: ['link'],
  methods: {
    go() {
      this.$router.push(this.link);
    }

  }
});
// CONCATENATED MODULE: ./components/card/cardBack.vue?vue&type=script&lang=js&
 /* harmony default export */ var card_cardBackvue_type_script_lang_js_ = (cardBackvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card/cardBack.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  card_cardBackvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3a835fd0",
  "109665de"
  
)

/* harmony default export */ var cardBack = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=card-back.js.map