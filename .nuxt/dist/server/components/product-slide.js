exports.ids = [21,14,18];
exports.modules = {

/***/ 144:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(170);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("23f1a6a7", content, true, context)
};

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_productSlide_vue_vue_type_style_index_0_id_57428e7c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(144);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_productSlide_vue_vue_type_style_index_0_id_57428e7c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_productSlide_vue_vue_type_style_index_0_id_57428e7c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_productSlide_vue_vue_type_style_index_0_id_57428e7c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_productSlide_vue_vue_type_style_index_0_id_57428e7c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".rounded[data-v-57428e7c]{border-radius:11px!important}.imgBox[data-v-57428e7c]{margin:15px;height:180px}a[data-v-57428e7c]:hover{border:0;text-decoration:none}.box-btn[data-v-57428e7c]{display:none}.box[data-v-57428e7c]::-webkit-scrollbar{width:0;height:0;background-color:hsla(0,0%,99.6%,.1)}.box[data-v-57428e7c]::-webkit-scrollbar-thumb{background-color:hsla(0,0%,96.1%,.3);border-radius:1px}.box[data-v-57428e7c]::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 1px rgba(241,238,238,.1)}.slider[data-v-57428e7c]::-webkit-scrollbar{width:0;height:0;background-color:hsla(0,0%,99.6%,.1)}.slider[data-v-57428e7c]::-webkit-scrollbar-thumb{background-color:hsla(0,0%,96.1%,.3);border-radius:1px}.slider[data-v-57428e7c]::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 1px rgba(241,238,238,.1)}.product[data-v-57428e7c]{margin:5%;font-size:17px}.product[data-v-57428e7c]:hover{box-shadow:0 .5rem 2rem rgba(0,0,0,.15)}.slider img[data-v-57428e7c]{position:relative;width:70%!important}.slider p[data-v-57428e7c]{font-size:14px;text-align:center}@media only screen and (max-width:1400px){.border[data-v-57428e7c]{border:0!important}.sliderBox[data-v-57428e7c]{box-shadow:none!important;background-color:#f5f5f5!important;border:none;margin:0!important;padding:0!important}.sliderBox p[data-v-57428e7c],h6[data-v-57428e7c]{font-size:13px!important}.sliderBox i[data-v-57428e7c]{padding-top:2px!important;font-size:22px!important}.product[data-v-57428e7c]{box-shadow:0 .4rem .7rem rgba(0,0,0,.15)!important;border:1px solid hsla(0,0%,85.9%,.59);min-width:150px}.sliderBoxItem[data-v-57428e7c]{min-width:140px!important}.sliderBoxItem p[data-v-57428e7c]{font-size:11px!important}.Special p[data-v-57428e7c],span[data-v-57428e7c]{font-size:13px!important}.Special img[data-v-57428e7c]{display:table;width:110px!important;margin:auto!important}.sp img[data-v-57428e7c]{max-height:200px!important}.card[data-v-57428e7c],.card-body[data-v-57428e7c]{padding:0!important;margin:0!important}.title[data-v-57428e7c]{font-size:14px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productSlide.vue?vue&type=template&id=57428e7c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._ssrNode("<div class=\" \" data-v-57428e7c>","</div>",[_vm._ssrNode("<div class=\" d-flex justify-content-center align-items-center col-12  p-0 mt-4 \" data-v-57428e7c>","</div>",[_vm._ssrNode("<div class=\"d-none d-xl-block ml-2\" style=\" color: #d1d6db \" data-v-57428e7c><i class=\"material-icons\" style=\"font-size: 40px\" data-v-57428e7c>navigate_next</i></div> "),_vm._ssrNode("<div class=\"sliderBox col-12   col-xl-11 m-0 px-2 bg-white shadow-sm border rounded\" data-v-57428e7c>","</div>",[_vm._ssrNode("<div class=\"d-flex pr-2 justify-content-between \" style=\"font-size: 9px\" data-v-57428e7c>","</div>",[_vm._ssrNode("<h6 class=\"pt-4  ml-4 pr-0 ml-0 font-weight-bold\" style=\"border-bottom: 1px solid darkred; font-size: 14px\" data-v-57428e7c>"+_vm._ssrEscape("\n            "+_vm._s(_vm.title)+"\n          ")+"</h6> "),_vm._ssrNode("<h6 class=\"flex-grow-1 pt-4 ml-2  text-left \" style=\"border-bottom: 1px solid rgba(174,173,173,0.62);cursor: pointer \" data-v-57428e7c>","</h6>",[_c('nuxt-link',{staticClass:"text-success ",attrs:{"to":_vm.link}},[_c('div',{staticClass:"d-flex justify-content-end align-items-center pb-3"},[_c('p',{staticClass:"m-0",staticStyle:{"font-size":"12px"}},[_vm._v("مشاهده لیست کامل")]),_vm._v(" "),_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"22px"}},[_vm._v("navigate_before")])])])],1)],2),_vm._ssrNode(" "),_vm._ssrNode("<div data-v-57428e7c>","</div>",[_vm._ssrNode("<div class=\"w-auto d-flex slider \" style=\"overflow-x: scroll\" data-v-57428e7c>","</div>",_vm._l((_vm.product),function(item,index2){return _vm._ssrNode("<div class=\"sliderBoxItem bg-white  mx-1 mx-md-2 my-3 px-1 py-3 rounded product\" style=\"min-width: 195px\" data-v-57428e7c>","</div>",[_c('nuxt-link',{attrs:{"to":'/product/' + item.slug}},[_c('div',{staticClass:"slider text-center"},[_c('img',{directives:[{name:"lazy-load",rawName:"v-lazy-load"},{name:"lazy-load",rawName:"v-lazy-load"}],attrs:{"alt":' قیمت خرید بیسمارک ' + item.name + item.model,"title":' قیمت خرید بیسمارک '  + item.name + item.model,"data-src":'https://server.bismark.ir'  + item.imageMini}}),_vm._v(" "),_c('ProductName',{attrs:{"name":item.name,"model":item.model}}),_vm._v(" "),_c('p',{staticClass:"mx-2 border-top"}),_vm._v(" "),_c('Price',{attrs:{"price":item.price,"available":item.available,"special":item.special}})],1)])],1)}),0)])],2),_vm._ssrNode(" <div class=\"d-none d-xl-block mr-2\" style=\" \" data-v-57428e7c><i class=\"material-icons\" style=\"font-size: 40px;  color: #d1d6db \" data-v-57428e7c>navigate_before</i></div>")],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/product/productSlide.vue?vue&type=template&id=57428e7c&scoped=true&

// EXTERNAL MODULE: ./components/product/productName.vue + 4 modules
var productName = __webpack_require__(87);

// EXTERNAL MODULE: ./components/product/price.vue + 4 modules
var price = __webpack_require__(96);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productSlide.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var productSlidevue_type_script_lang_js_ = ({
  props: ['product', 'title', 'link'],
  name: "productSlide",
  components: {
    Price: price["default"],
    ProductName: productName["default"]
  }
});
// CONCATENATED MODULE: ./components/product/productSlide.vue?vue&type=script&lang=js&
 /* harmony default export */ var product_productSlidevue_type_script_lang_js_ = (productSlidevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/product/productSlide.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(169)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  product_productSlidevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "57428e7c",
  "62b674de"
  
)

/* harmony default export */ var productSlide = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ProductName: __webpack_require__(87).default})


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(94);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("97dae168", content, true, context)
};

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productName.vue?vue&type=template&id=23255a75&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"text-center"},[_vm._ssrNode("<p class=\"text-center  pt-1 pt-md-3 font-weight-bold\" style=\"font-size: 14px; height: 50px\" data-v-23255a75>"+_vm._ssrEscape("\n    "+_vm._s(_vm.name)+" مدل "+_vm._s(_vm.model)+"\n  ")+"</p>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/product/productName.vue?vue&type=template&id=23255a75&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/productName.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
/* harmony default export */ var productNamevue_type_script_lang_js_ = ({
  name: "productName",
  props: ['name', 'model']
});
// CONCATENATED MODULE: ./components/product/productName.vue?vue&type=script&lang=js&
 /* harmony default export */ var product_productNamevue_type_script_lang_js_ = (productNamevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/product/productName.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  product_productNamevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "23255a75",
  "75d700db"
  
)

/* harmony default export */ var productName = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_price_vue_vue_type_style_index_0_id_2697b85a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(86);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_price_vue_vue_type_style_index_0_id_2697b85a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_price_vue_vue_type_style_index_0_id_2697b85a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_price_vue_vue_type_style_index_0_id_2697b85a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_price_vue_vue_type_style_index_0_id_2697b85a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 94:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "del[data-v-2697b85a]{font-size:13px;color:red}@media only screen and (max-width:720px){div span[data-v-2697b85a]{font-size:12px!important}del span[data-v-2697b85a]{font-size:10px!important}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/price.vue?vue&type=template&id=2697b85a&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"m-0 p-0 position-relative "},[_vm._ssrNode(((_vm.available === 'true')?("<div class=\"text-center\" data-v-2697b85a><div class=\" d-flex justify-content-center align-items-center \" data-v-2697b85a>"+((_vm.special !== 0)?("<del class=\" d-flex justify-content-center align-items-center \" data-v-2697b85a><span class=\"font-weight-bold\" style=\"font-size: 12px\" data-v-2697b85a>"+_vm._ssrEscape(_vm._s(_vm._f("formatNumber")(_vm.price)))+"</span> <span class=\" \" style=\"font-size: 12px;\" data-v-2697b85a>تومان</span></del>"):"<!---->")+"</div> "+((_vm.special !== 0)?("<div class=\"special d-flex align-items-center\" style=\"position: absolute; top: -45px; left: 0px\" data-v-2697b85a><span class=\"pt-1 pl-1 font-weight-bold\" style=\"font-size: 12px;color: #ff0042\" data-v-2697b85a>"+_vm._ssrEscape("(%"+_vm._s(_vm.special)+")")+"</span> <i class=\"material-icons\" style=\"color: #FFD700\" data-v-2697b85a>grade</i></div>"):"<!---->")+" <div class=\"text-success d-flex justify-content-center align-items-center pt-3 \" data-v-2697b85a>"+((_vm.special === 0)?("<span style=\"font-size:15px;font-weight: 700;\" data-v-2697b85a>"+_vm._ssrEscape(_vm._s(_vm._f("formatNumber")(_vm.price)))+"</span>"):("<span style=\"font-size:15px;font-weight: 700;\" data-v-2697b85a>"+_vm._ssrEscape(_vm._s(_vm._f("formatNumber")(_vm.price - ( _vm.price * (_vm.special / 100 )))))+"</span>"))+" <span class=\"pr-2 \" style=\"font-size: 12px;\" data-v-2697b85a>تومان</span></div></div>"):("<div class=\"text-center   justify-content-center align-items-center  \" data-v-2697b85a><div class=\" text-center  d-flex justify-content-center align-items-center\" data-v-2697b85a><i class=\"material-icons p-0 text-info\" data-v-2697b85a>shopping_bag</i> <span class=\"text-center font-weight-bold text-info pr-1 pt-1\" style=\" position: relative; bottom: 0\" data-v-2697b85a>بزودی</span></div> <div class=\"text-success d-flex justify-content-center align-items-center pt-2 \" data-v-2697b85a><span style=\"font-size:15px;font-weight: 700;color: #808080\" data-v-2697b85a>"+_vm._ssrEscape(_vm._s(_vm._f("formatNumber")(_vm.price)))+"</span> <span class=\"pr-2 \" style=\"font-size: 12px;color: #808080\" data-v-2697b85a>تومان</span></div></div>")))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/product/price.vue?vue&type=template&id=2697b85a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/product/price.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var pricevue_type_script_lang_js_ = ({
  props: {
    price: {},
    available: {
      type: String,
      default: 'true'
    },
    special: {
      default: 'false'
    }
  },
  name: "price"
});
// CONCATENATED MODULE: ./components/product/price.vue?vue&type=script&lang=js&
 /* harmony default export */ var product_pricevue_type_script_lang_js_ = (pricevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/product/price.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(93)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  product_pricevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2697b85a",
  "6b32a94a"
  
)

/* harmony default export */ var price = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=product-slide.js.map