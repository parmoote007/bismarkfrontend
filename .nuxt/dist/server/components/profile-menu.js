exports.ids = [25];
exports.modules = {

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/profile/profileMenu.vue?vue&type=template&id=7130eec3&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"border rounded shadow p-3"},[_c('nuxt-link',{attrs:{"to":"/profile"}},[_c('div',{staticClass:"d-flex align-items-center pb-3 border-bottom"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"60px","color":"#dddddd"}},[_vm._v("account_circle")]),_vm._v(" "),_c('div',{staticClass:"mr-3  "},[_c('p',{staticClass:"p-0 m-0"},[_vm._v("\n          "+_vm._s(_vm.user.name)+"\n        ")]),_vm._v(" "),_c('p',{staticClass:"pt-1 m-0",staticStyle:{"font-size":"12px","color":"#808080"}},[_vm._v(_vm._s(_vm.user.phoneNumber))])])])]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"menu mt-3 border-bottom\" data-v-7130eec3>","</div>",[_c('nuxt-link',{attrs:{"to":"/card/cardList"}},[_c('div',{staticClass:"d-flex  p-2"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"29px"}},[_vm._v("list_alt")]),_vm._v(" "),_c('p',{staticClass:"pr-3 pt-1 font-weight-bold",staticStyle:{"font-size":"13px","color":"#646363"}},[_vm._v("سفارش های من")])])]),_vm._ssrNode(" <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>favorite_border</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>مورد علاقه ها</p></div> <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>chat_bubble_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>نظرات</p></div> "),_c('nuxt-link',{attrs:{"to":"/profile/address"}},[_c('div',{staticClass:"d-flex  p-2"},[_c('i',{staticClass:"material-icons",staticStyle:{"font-size":"29px"}},[_vm._v("house_siding")]),_vm._v(" "),_c('p',{staticClass:"pr-3 pt-1 font-weight-bold",staticStyle:{"font-size":"13px","color":"#646363"}},[_vm._v("آدرس")])])]),_vm._ssrNode(" <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>mail_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>پیغام ها</p></div> <div class=\"d-flex  p-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>person_outline</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3> اطلاعات حساب</p></div>")],2),_vm._ssrNode(" <div class=\"d-flex  pt-4 pr-2\" data-v-7130eec3><i class=\"material-icons\" style=\"font-size: 29px\" data-v-7130eec3>logout</i> <p class=\"pr-3 pt-1 font-weight-bold\" style=\"font-size: 13px; color: #646363\" data-v-7130eec3>خروج</p></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/profile/profileMenu.vue?vue&type=template&id=7130eec3&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/string-replace-loader??ref--12!./components/profile/profileMenu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var profileMenuvue_type_script_lang_js_ = ({
  name: "profileMenu",
  computed: {
    user() {
      return this.$store.state.userInfo;
    }

  }
});
// CONCATENATED MODULE: ./components/profile/profileMenu.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_profileMenuvue_type_script_lang_js_ = (profileMenuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/profile/profileMenu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(115)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_profileMenuvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7130eec3",
  "39d67d23"
  
)

/* harmony default export */ var profileMenu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(97);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_node_modules_string_replace_loader_index_js_ref_12_profileMenu_vue_vue_type_style_index_0_id_7130eec3_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 116:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".menu p[data-v-7130eec3]:hover{color:#3b8070}.menu i[data-v-7130eec3],.menu p[data-v-7130eec3]{cursor:pointer}a[data-v-7130eec3]{text-decoration:none;color:#646363!important}a[data-v-7130eec3]:hover,a:hover p[data-v-7130eec3]{text-decoration:none;color:#0fabc6!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(116);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("571f3da1", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=profile-menu.js.map