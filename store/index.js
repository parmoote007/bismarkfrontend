export const state = () => ({
  user: {
    id: null,
    api_token: null,
    name: null,
    status: null
  },
  userInfo : {},
  auth: false,
  authStatus: 'login',
  card: [],
  cardTotal: 0,
  cardDiscountPrice : 0,
  cardAddressStatus: false,
  cardAddress: [],
  cardSendMode : false,
  paymentMode : false,
  discountCode : 0,
  discount : '',

  productList : null,
  stateList : null,
})

export const mutations = {
  EDIT_USER(state, payload) {
    if (payload === null) {
      state.user = {
        id: null,
        api_token: null,
        name: null,
        status: null
      };
      state.auth = false;
    } else {
      state.user = {
        id: payload.id,
        api_token: payload.api_token,
        name: payload.name,
        status: payload.status
      }
    }
  },

  EDIT_USER_INFO (state,payload)
  {
    state.userInfo = payload;
  },

  EDIT_AUTH(state, payload) {
    state.auth = payload;
  },

  EDIT_AUTH_STATUS(state, payload) {
    state.authStatus = payload;
  },

  ADD_TO_CARD(state, payload) {
    state.card = JSON.parse(localStorage.getItem('card'));

    let add = false;

   if (state.card.length === 0){
     add = true;
   }else {
     let products = state.card;
     for (let i = 0; i < products.length; i++) {
       if (products[i].id === payload.id) {
         products[i].con++;
         i = 1000;
         add = false;
       } else {
         add = true;
       }
     }
   }

    if (add) {
      payload.con = 1;
      state.card.push(payload);
    }

    localStorage.setItem('card', JSON.stringify(state.card));

    let total = 0;

    let discountPrice = 0;

    let product = state.card;

    for (let i = 0; i < product.length; i++) {
      if (product[i].special !== 0){
        discountPrice = discountPrice + ((product[i].price * product[i].con) * (product[i].special / 100));
        total = total + (product[i].price * product[i].con);
      }else {
        total = total + (product[i].price * product[i].con);

      }
    }
    state.cardTotal = total;
    state.cardDiscountPrice = discountPrice;
  },

  DELETE_IN_CARD (state, payload){
    state.card = JSON.parse(localStorage.getItem('card'));

    state.card.splice(payload,1);

    localStorage.setItem('card', JSON.stringify(state.card));
  },

  CARD_TOTAL_UPDATE(state, payload) {
    state.cardTotal = payload;
  },

    CARD_DISCOUNT_PRICE_UPDATE(state, payload) {
    state.cardDiscountPrice = payload;
  },

  UPDATE_CARD(state, payload) {
    state.card = payload;
  },

  EDIT_CARD_PRODUCT_CON (state,payload){
    state.card = JSON.parse(localStorage.getItem('card'));

    if (payload[1] === '+') {
      state.card[payload[0]].con++;
    }
    if (payload[1] === '-') {
      if ( state.card[payload[0]].con === 1){
        return false;
      }

      state.card[payload[0]].con--;
    }

    localStorage.setItem('card', JSON.stringify(state.card));
  },

  CLEAR_CARD(state) {
    state.card = [];
  },

  EDIT_CARD_ADDRESS (state, payload){
    state.cardAddress = payload;
  },

  EDIT_PRODUCTS (state, payload){
    state.productList = payload;
  },
  EDIT_STATE_LIST (state, payload){
    state.stateList = payload;
  },

 EDIT_CARD_ADDRESS_STATUS (state, payload){
    state.cardAddressStatus = payload;
  },

  EDIT_CARD_SEND_MODE (state,payload) {
    state.cardSendMode = payload;
  },

  EDIT_CARD_PAYMENT_MODE (state,payload) {
    state.paymentMode = payload;
  },

  UPDATE_DISCOUNT (state, payload){
    state.discount = payload;
  },

  UPDATE_DISCOUNT_CODE (state, payload){
    state.discountCode = payload;

    let product = state.card;

    for (let i = 0; i < product.length; i++) {
      if (payload === 40){
        product[i].special = 'true';
      }
    }

    state.card = product;
  }

}

export const actions = {
  getCard({commit}) {
    if (localStorage.getItem('card')) {
      commit('UPDATE_CARD', JSON.parse(localStorage.getItem('card')));
    } else {
      localStorage.setItem('card', JSON.stringify([]));
    }
  },

  updateCardTotal({commit, state}) {

    let total = 0;

    let discountPrice = 0;

    let product = state.card;

    for (let i = 0; i < product.length; i++) {
      if (product[i].special !==0){
        discountPrice = discountPrice + ((product[i].price * product[i].con) * (product[i].special / 100));
        total = total + (product[i].price * product[i].con);
      }else {
        total = total + ((product[i].price * product[i].con) );

      }
    }

    commit('CARD_TOTAL_UPDATE', total);
    commit('CARD_DISCOUNT_PRICE_UPDATE', discountPrice);
  }



}
