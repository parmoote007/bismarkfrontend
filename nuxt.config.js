module.exports = {

  // devServer: { proxy: 'https://server.bismark.ir' },

  // outputDir should be Laravel public dir
  // outputDir: '../../../public/',
  // publicPath: '/',

  // for production we use blade template file
  // indexPath: process.env.NODE_ENV === 'generate'
  //   ? '../resources/views/app.blade.php'
  //   : 'index.html',

  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',


  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: ' بیسمارک',
    meta: [
      { charset: 'utf-8' },
      { lang: 'fa' },
      { dir: 'rtl' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '', dir : 'rtl' },
    ],
    link: [
      { rel: 'icon', href: '/iconTop.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'},
    ]
  },



  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'bootstrap/dist/css/bootstrap.min.css',
    '@/assets/all.css',
  ],

  style : {
    '* ' :  'font-family: yekan'

  },


  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/price.js',
    {src : '@/plugins/PDatePicker.js',  mode: 'client'},
    {src : '@/plugins/menuTop.js',  mode: 'client'},

  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/pwa',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/axios',
    '@nuxtjs/bootstrap-vue',
    '@nuxtjs/toast',
    [
      'nuxt-lazy-load', {
      images: false,
      videos: true,
      audios: true,
      iframes: true,
    }
    ],
  ],

  toast: {
    position: 'top-center',
    register: [ // Register custom toasts
      {
        name: 'my-error',
        message: 'Oops...Something went wrong',
        options: {
          type: 'error'
        }
      }
    ]
  },

  ssr : true,

  router : {
    base : '/',
  },

  axios: {
    proxy: true,
    host : 'https://server.bismark.ir',
    baseURL: 'https://server.bismark.ir', // Used as fallback if no runtime config is provided
    browserBaseURL: 'https://server.bismark.ir', // Used as fallback if no runtime config is provided
  },

  proxy: {
    '/api/': 'https://server.bismark.ir',
    // 'https://api.zarinpal.com':{target: 'https://api.zarinpal.com', changeOrigin: true},
    '/api2/': 'https://api.another-website.com',
  },


  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      title: 'bismark',
      author: 'Me',
    },
    manifest: {
      name: 'bismark',
      short_name: 'bismark',
      lang: 'fa',
      display: 'standalone',
    },
    icon : {
      source: 'icon5.jpg',
      fileName : 'icon5.jpg',
      sizes : [64, 120, 144, 152, 192, 384, 512],
      targetDir : 'icon5.jpg',
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // transpile: ['PDatePicker']
  },

server: {
    host: "0.0.0.0",
    port:  "8685"
  }

}
