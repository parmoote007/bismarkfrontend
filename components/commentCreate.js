import Http from 'axios';
import Notification from "~/plugins/Notification";
// import LoadingView from "../components/admin/LoadingView";

let testT = 'test';

class Comment {

    static async create(item) {
        let testT = 'test';
        Notification.showLoading();
        await Http.post('/api/V1/serviceList/comment/create', item)
            .then(response => {
                console.log(response);
                Notification.hideLoading();
                testT = true;
            })
            .catch(error => {
                console.log(error.response);
                testT = false;
                Notification.error('مشکلی در اجرای عملیات به وجود آمد');
            });

        return testT;
    }

}

export default Comment;
