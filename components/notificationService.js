import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

class Notification {
    static success (msg) {
        this.hideLoading();
        return toastr.success(msg);
    }

    static error (msg) {
        this.hideLoading();
        return toastr.error(msg);
    }

    static info (msg) {
        toastr.info(msg);
    }

    static serverError (status) {
        if (status === 500) {
            this.hideLoading();
            return toastr.warning('ارتباط با سرور برقرار نشد.');
        }

        if (status === 401) {
            this.hideLoading();

            toastr.error('برای دست رسی باید وارد شوید.');
        }

        if (status === 403) {
            this.hideLoading();
            toastr.error('شما دسترسی لازم برای ورود به این بخش را ندارید.');
        }


        if (status === 404) {
            this.hideLoading();
            return toastr.error('صفحه مورد نظر یافت نشد.');
        }

        if (status === 422) {
            this.hideLoading();
            return toastr.error('اطلاعات به درستی وارد نشده است.');
        }
    }

    static showLoading() {
        if (document.getElementById('loading')) {
            document.getElementById('loading').style.display='block';
        }
    }

    static hideLoading() {
        if (document.getElementById('loading')) {
            document.getElementById('loading').style.display='none';
        }
    }

    static title (text) {
        document.title = text;
    }

    static description (text) {
        let descEl = document.querySelector('head meta[name="Description"]');
        descEl.setAttribute('content', text);
    }

}

export default Notification;
